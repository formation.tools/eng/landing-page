import type { InputFormProps } from '@/components/InputForm'
import { PortableTextBlock } from '@portabletext/types'

type FullScreenshotHeroSanityData = {
  _type: 'fullScreenshotHero'
  _id: string
  heroTitle: string
  heroSubtitle: string
  heroCtaText: string
  heroCtaLink: string
  image: string
}

type heroSimpleCenteredSanityData = {
  _type: 'heroSimpleCentered'
  _id: string
  headline: string
  navBarData: navBarSanityData
}

type navBarSanityData = {
  _type: 'navBar'
  _id: string
  companyName: string
  logo: {
    asset: {
      url: string
    }
  }
  navigation: Array<{
    linkName: string
    linkUrl: string
  }>
}

type PricingNewSanityData = {
  _type: 'pricingNew'
  _id: string
  frequencies: Array<{
    value: string
    label: string
    priceSuffix: string
  }>
  tiers: Array<{
    name: string
    id: string
    href: string
    price: {
      monthly: string
      annually: string
    }
    description: string
    features: string[]
    mostPopular: boolean
    mostPopularLabel?: string
  }>
}

type FormSingleInputHorizontalSanityData = {
  _type: 'formSingleInputHorizontal'
  _id: string
  title: string
  subtitle?: string
  thanksLabel: string
  tryAgainLabel: string
  form: InputFormProps
  formData:
    | 'WaitlistRegistrationFormData'
    | 'IntegrationSuggestionFormData'
    | 'PlatformSuggestionFormData'
}

type QuestionsNewSanityData = {
  _type: 'questionsNew'
  _id: string
  isDark: boolean
  questions: Array<{
    question: string
    answer: string
  }>
}

type LogoCloudLightTextOnTopRowSanityData = {
  _type: 'logoCloudLightTextOnTopRow'
  _id: string
  headline: string
  text: string
  CTA: {
    buttonOne: string
    buttonTwo: string
  }
  logos: Array<{
    asset: {
      url: string
    }
  }>
}

type FeatureWithProductSanityData = {
  _type: 'featureWithProduct'
  _id: string
  tagline: string
  headline: string
  text: string
  features: Array<{
    name: string
    description: string
    icon?: {
      asset: {
        url: string
      }
    }
  }>
  ProductShot: {
    asset: {
      url: string
    }
  }
}

type TermsAndConditionsSanityData = {
  _type: 'termsAndConditions'
  _id: string
  title: string
  portableTextContent: PortableTextBlock[]
}

type PrivacyPolicySanityData = {
  _type: 'privacyPolicy'
  _id: string
  title: string
  portableTextContent: PortableTextBlock[]
}

export type Element =
  | FullScreenshotHeroSanityData
  | PricingNewSanityData
  | FormSingleInputHorizontalSanityData
  | QuestionsNewSanityData
  | LogoCloudLightTextOnTopRowSanityData
  | TermsAndConditionsSanityData
  | PrivacyPolicySanityData
  | heroSimpleCenteredSanityData
  | FeatureWithProductSanityData
