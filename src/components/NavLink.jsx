import Link from 'next/link'

export function NavLink({ href, children }) {
  return (
    <Link
      href={href}
      className="inline-block rounded-lg py-1 px-2 text-sm text-black hover:bg-black hover:text-green-300"
    >
      {children}
    </Link>
  )
}
