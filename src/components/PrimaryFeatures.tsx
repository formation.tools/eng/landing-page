import React from 'react'
import { useContext } from 'react'
import { ThemeContext } from './ThemeContext'
import { useEffect, useState } from 'react'
import Image from 'next/image'
import { Tab } from '@headlessui/react'
import clsx from 'clsx'

import { Container } from '@/components/Container'
import backgroundImage from '@/images/background-features.jpg'

// TODO: Remove screenshots or update
import screenshotExpenses from '@/images/screenshots/expenses.png'
import screenshotPayroll from '@/images/screenshots/payroll.png'
import screenshotReporting from '@/images/screenshots/reporting.png'
import screenshotVatReturns from '@/images/screenshots/vat-returns.png'

import {
  ArrowPathIcon,
  CloudArrowUpIcon,
  Cog6ToothIcon,
  FingerPrintIcon,
  LockClosedIcon,
  ServerIcon,
} from '@heroicons/react/20/solid'

type FeaturesListProp = {
  features: Array<{
    name: string
    description: string
    icon?: any // FIXME: Type correctly
  }>
}

type FeaturesContainerProp = {
  title?: string
  punchline?: string
  description?: string
  isFeaturedInline?: boolean
  isWithGradient?: boolean
}

type WithScreenshot = {
  screenshot: string | any // FIXME: Type correctly
}

type FeaturesProp = FeaturesListProp & FeaturesContainerProp

export function PrimaryFeatures({ features }: FeaturesListProp) {
  let [tabOrientation, setTabOrientation] = useState('horizontal')

  useEffect(() => {
    let lgMediaQuery = window.matchMedia('(min-width: 1024px)')

    function onMediaQueryChange({ matches }) {
      setTabOrientation(matches ? 'vertical' : 'horizontal')
    }

    onMediaQueryChange(lgMediaQuery)
    lgMediaQuery.addEventListener('change', onMediaQueryChange)

    return () => {
      lgMediaQuery.removeEventListener('change', onMediaQueryChange)
    }
  }, [])

  const theme = useContext(ThemeContext)

  return (
    <section
      id="features"
      aria-label="Features for running your books"
      className="relative overflow-hidden bg-black pt-20 pb-28 sm:py-32"
    >
      {/*
      <Image
        className="absolute top-1/2 left-1/2 max-w-none translate-x-[-44%] translate-y-[-42%]"
        src={backgroundImage}
        alt=""
        width={2245}
        height={1636}
        unoptimized
      />
      */}
      <Container className="relative">
        <div className="max-w-2xl md:mx-auto md:text-center xl:max-w-none">
          <h2 className="font-display text-3xl tracking-tight text-green-400 sm:text-4xl md:text-5xl">
            Power-tools to build hard tech!
          </h2>
          <p className={`mt-6 text-lg tracking-tight ${theme.textPrimary}`}>
            Staying out of your way so you can remain in flow and crack those
            hard problems.
          </p>
        </div>
        <Tab.Group
          as="div"
          className="mt-16 grid grid-cols-1 items-center gap-y-2 pt-10 sm:gap-y-6 md:mt-20 lg:grid-cols-12 lg:pt-0"
          vertical={tabOrientation === 'vertical'}
        >
          {({ selectedIndex }) => (
            <>
              <div className="-mx-4 flex overflow-x-auto pb-4 sm:mx-0 sm:overflow-visible sm:pb-0 lg:col-span-5">
                <Tab.List className="relative z-10 flex gap-x-4 whitespace-nowrap px-4 sm:mx-auto sm:px-0 lg:mx-0 lg:block lg:gap-x-0 lg:gap-y-1 lg:whitespace-normal">
                  {features.map((feature, featureIndex) => (
                    <div
                      key={feature.name}
                      className={clsx(
                        'group/item',
                        'opacity-50',
                        'hover:opacity-100',
                        'group relative rounded-full py-1 px-4 lg:rounded-r-none lg:rounded-l-xl lg:p-6',
                        selectedIndex === featureIndex
                          ? 'bg-white lg:bg-white/5 lg:ring-2 lg:ring-inset lg:ring-green-400/10'
                          : 'hover:bg-white/10 lg:hover:bg-white/5',
                      )}
                    >
                      <h3>
                        <Tab
                          className={clsx(
                            'font-display text-lg font-black [&:not(:focus-visible)]:focus:outline-none',
                            selectedIndex === featureIndex
                              ? `${theme.textPrimary} lg:${theme.textPrimary}`
                              : 'text-green-400 hover:text-green-400 lg:text-green-400',
                          )}
                        >
                          <span className="absolute inset-0 rounded-full lg:rounded-r-none lg:rounded-l-xl" />
                          <span className="group-hover/item:before:content-['>_']">
                            {feature.name}
                          </span>
                        </Tab>
                      </h3>
                      <p
                        className={clsx(
                          'mt-2 hidden text-sm lg:block',
                          selectedIndex === featureIndex
                            ? 'text-green-400'
                            : `${theme.textPrimary} group-hover:${theme.textPrimary}`,
                        )}
                      >
                        {feature.description}
                      </p>
                    </div>
                  ))}
                </Tab.List>
              </div>
              <Tab.Panels className="lg:col-span-7">
                {features.map((feature) => (
                  <Tab.Panel key={feature.name} unmount={false}>
                    <div className="relative sm:px-6 lg:hidden">
                      <div className="absolute -inset-x-4 top-[-6.5rem] bottom-[-4.25rem] bg-white/10 ring-1 ring-inset ring-white/10 sm:inset-x-0 sm:rounded-t-xl" />
                      <p className="relative mx-auto max-w-2xl text-base text-white sm:text-center">
                        {feature.description}
                      </p>
                    </div>
                    {/*
                    <div className="mt-10 w-[45rem] overflow-hidden rounded-xl bg-slate-50 shadow-xl shadow-green-900/20 sm:w-auto lg:mt-0 lg:w-[67.8125rem]">
                      <Image
                        className="w-full"
                        src={feature.image}
                        alt=""
                        priority
                        sizes="(min-width: 1024px) 67.8125rem, (min-width: 640px) 100vw, 45rem"
                      />
                    </div>*/}
                  </Tab.Panel>
                ))}
              </Tab.Panels>
            </>
          )}
        </Tab.Group>
      </Container>
    </section>
  )
}

export function LargeScreenshotFeatures({
  features,
  title,
  punchline,
  description,
  screenshot,
  isFeaturedInline = true,
  isWithGradient = true,
}: FeaturesProp & WithScreenshot) {
  const theme = useContext(ThemeContext)
  const Screenshot = typeof screenshot === 'string' ? undefined : screenshot

  return (
    <div className={`${theme.background} py-24 sm:py-32`}>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-2xl sm:text-center">
          {title && (
            <h2
              className={`${theme.textAccent} font-semibold leading-8 tracking-tight`}
            >
              {title}
            </h2>
          )}
          {punchline && (
            <p
              className={`${theme.textPrimary} mt-2 text-3xl font-bold tracking-tight sm:text-4xl`}
            >
              {punchline}
            </p>
          )}
          {description && (
            <p className={`${theme.textSecondary} mt-6 text-lg leading-8`}>
              {description}
            </p>
          )}
        </div>
      </div>
      <div className="relative overflow-hidden pt-16">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          {Screenshot && (
            <Screenshot
              viewBox="0 0 1306 640"
              className="h-full w-full object-contain"
            />
          )}
          {isWithGradient && (
            <div className="relative" aria-hidden="true">
              <div
                className={`${theme.gradientEdge} absolute -inset-x-20 bottom-0 pt-[7%]`}
              />
            </div>
          )}
        </div>
      </div>
      {/* <div className="mx-auto mt-16 max-w-7xl px-6 sm:mt-20 md:mt-24 lg:px-8">
        <dl
          className={`${theme.textSecondary} mx-auto grid max-w-2xl grid-cols-1 gap-x-6 gap-y-10 text-base leading-7 sm:grid-cols-2 lg:mx-0 lg:max-w-none lg:grid-cols-3 lg:gap-x-8 lg:gap-y-16`}
        >
          {features.map((feature) => (
            <div key={feature.name} className="relative pl-9">
              <dt className={`${theme.textPrimary} inline font-semibold`}>
                {feature.icon !== undefined && (
                  <feature.icon
                    className={`absolute top-1 left-1 h-5 w-5 ${theme.icon}`}
                    aria-hidden="true"
                  />
                )}
                {feature.name}
              </dt>
              <dd className={isFeaturedInline ? 'inline' : ''}>
                {feature.description}
              </dd>
            </div>
          ))}
        </dl>
      </div> */}
    </div>
  )
}

export function SideScreenshotFeatures({
  features,
  title,
  punchline,
  description,
  screenshot,
}: FeaturesProp & WithScreenshot) {
  const theme = useContext(ThemeContext)
  return (
    <div className={`${theme.background} sm:py-32' overflow-hidden py-24`}>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto grid max-w-2xl grid-cols-1 gap-y-16 gap-x-8 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
          <div className="lg:pr-8 lg:pt-4">
            <div className="lg:max-w-lg">
              {title && (
                <h2
                  className={`${theme.textAccent} text-lg font-semibold leading-8 tracking-tight`}
                >
                  {title}
                </h2>
              )}
              {punchline && (
                <p
                  className={`${theme.textPrimary} mt-2 text-3xl font-bold tracking-tight sm:text-4xl`}
                >
                  {punchline}
                </p>
              )}
              {description && (
                <p className={`${theme.textSecondary} mt-6 text-lg leading-8`}>
                  {description}
                </p>
              )}
              <dl
                className={`${theme.textSecondary} mt-10 max-w-xl space-y-8 text-base leading-7 lg:max-w-none`}
              >
                {features.map((feature) => (
                  <div key={feature.name} className="relative pl-9">
                    <dt className={`${theme.textPrimary} inline font-semibold`}>
                      {feature.icon !== undefined && (
                        <feature.icon
                          className={`${theme.icon} absolute top-1 left-1 h-5 w-5`}
                          aria-hidden="true"
                        />
                      )}{' '}
                      {feature.name}
                    </dt>{' '}
                    <dd className="inline">{feature.description}</dd>
                  </div>
                ))}
              </dl>
            </div>
          </div>
          <Image
            src={screenshot}
            alt="Product screenshot"
            className={`${theme.ringScreenshot} w-[48rem] max-w-none rounded-xl shadow-xl ring-1 ring-4 sm:w-[57rem] md:-ml-4 lg:-ml-0`}
            width={2432}
            height={1442}
          />
        </div>
      </div>
    </div>
  )
}
