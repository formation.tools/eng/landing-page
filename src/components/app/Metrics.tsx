import { ArrowDownIcon, ArrowUpIcon } from '@heroicons/react/20/solid'

type Statistic = {
  name: string
  current: string
  previous: string
  change: string
  changeType: string
}

type SelectorOption = {
  id: string
  name: string
  icon?: any
  isActive: boolean
  tabIndex: number
  ariaControlsId: string
}

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

type SelectorProps = {
  className?: string
  options: SelectorOption[]
}

type MetricsProps = {
  periods: SelectorOption[]
  stats: Statistic[]
}

export function Selector({ options }: SelectorProps) {
  const buttonActive = 'bg-white'
  const buttonInactive = 'bg-gray-200 text-gray-500 hover:text-black'

  const defaultValue = options.find((tab) => tab.isActive)

  if (defaultValue === undefined) {
    return null
  }

  return (
    <div>
      <div className="sm:hidden">
        <label htmlFor="tabs" className="sr-only">
          Select a tab
        </label>
        {/* Use an "onChange" listener to redirect the user to the selected tab URL. */}
        <select
          id="tabs"
          name="tabs"
          className="block w-full rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500"
          defaultValue={defaultValue.name}
        >
          {options.map((tab) => (
            <option key={tab.name}>{tab.name}</option>
          ))}
        </select>
      </div>
      <div className="hidden rounded-lg bg-gray-200 p-1 sm:block md:flex">
        <nav className="flex space-x-1" aria-label="Tabs">
          {options.map((tab) => (
            <a
              key={tab.name}
              href={'#'}
              className={classNames(
                'rounded-md px-3 py-2 text-sm font-medium',
                tab.isActive ? buttonActive : buttonInactive,
                //tab.isActive ? 'bg-indigo-100 text-indigo-700' : 'text-gray-500 hover:text-gray-700',
              )}
              aria-current={tab.isActive ? 'page' : undefined}
            >
              {tab.name}
            </a>
          ))}
        </nav>
      </div>
    </div>
  )
}

type MetricsHeadingProps = {
  title: string
  periods: SelectorOption[]
}
export function Heading({ title, periods }: MetricsHeadingProps) {
  return (
    <div className="md:flex">
      <h3 className="items-center align-baseline font-semibold md:flex md:flex-auto">
        Last {title}
      </h3>
      <Selector className={'md:flex-none'} options={periods} />
    </div>
  )
}

type StatsProps = {
  stats: Statistic[]
}
export function Statsbar({ stats }: StatsProps) {
  return (
    <dl className="mt-5 grid grid-cols-1 divide-y divide-gray-200 overflow-hidden rounded-lg bg-white shadow ring-1 ring-gray-200 md:grid-cols-4 md:divide-x md:divide-y-0">
      {stats.map((item, idx) => (
        <div key={item.name} className="px-4 py-5 sm:p-6">
          <dt className="text-base font-normal text-gray-900">{item.name}</dt>
          <dd className="mt-1 flex items-baseline justify-between md:block lg:flex">
            <div className="flex items-baseline text-2xl font-semibold text-indigo-600">
              {item.current}
              <span className="ml-2 text-sm font-medium text-gray-500">
                from {item.previous}
              </span>
            </div>

            <div
              className={classNames(
                item.changeType === 'increase'
                  ? 'bg-green-100 text-green-800'
                  : 'bg-red-100 text-red-800',
                'inline-flex items-baseline rounded-full px-2.5 py-0.5 text-sm font-medium md:mt-2 lg:mt-0',
              )}
            >
              {item.changeType === 'increase' ? (
                <ArrowUpIcon
                  className="-ml-1 mr-0.5 h-5 w-5 flex-shrink-0 self-center text-green-500"
                  aria-hidden="true"
                />
              ) : (
                <ArrowDownIcon
                  className="-ml-1 mr-0.5 h-5 w-5 flex-shrink-0 self-center text-red-500"
                  aria-hidden="true"
                />
              )}

              <span className="sr-only">
                {' '}
                {item.changeType === 'increase'
                  ? 'Increased'
                  : 'Decreased'} by{' '}
              </span>
              {item.change}
            </div>
          </dd>
        </div>
      ))}
    </dl>
  )
}

export default function Metrics({ stats, periods }: MetricsProps) {
  return (
    <div>
      <Heading title={'30 days'} periods={periods} />
      <Statsbar stats={stats} />
    </div>
  )
}
