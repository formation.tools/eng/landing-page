import {
  PropsWithChildren,
  Dispatch,
  SetStateAction,
  Fragment,
  useContext,
  useState,
  ReactNode,
} from 'react'
import { Dialog, Transition } from '@headlessui/react'

import { ThemeContext } from '@/components/ThemeContext'

import { Sidebar } from '@/components/app/Sidebar'

import {
  Bars3Icon,
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  ScaleIcon,
  ShieldCheckIcon,
  CircleStackIcon,
  BeakerIcon,
  SparklesIcon,
  HomeIcon,
  UsersIcon,
  XMarkIcon,
} from '@heroicons/react/24/outline'

const defaultNavigation = [
  { name: 'Dashboard', href: '#', icon: HomeIcon, current: true },
  { name: 'Contexts', href: '#', icon: FolderIcon, current: false },
  { name: 'Team', href: '#', icon: UsersIcon, current: false },
  { name: 'Playground', href: '#', icon: SparklesIcon, current: false },
  { name: 'Experiments', href: '#', icon: BeakerIcon, current: false },
  { name: 'Data sources', href: '#', icon: CircleStackIcon, current: false },
  { name: 'Policies', href: '#', icon: ShieldCheckIcon, current: false },
  {
    name: 'Prompt Templates',
    href: '#',
    icon: DocumentDuplicateIcon,
    current: false,
  },
  { name: 'Reports', href: '#', icon: ChartPieIcon, current: false },
]

const defaultTeams = [
  {
    id: '1',
    name: 'Customer Support',
    href: '#',
    initial: 'CS',
    current: false,
  },
  { id: '2', name: 'R&D', href: '#', initial: 'X', current: false },
  { id: '3', name: 'Dealsourcing', href: '#', initial: 'S', current: false },
]

type NavigationProps = {
  name: string
  href: string
  icon?: any
  current: boolean
}
const secondaryNavigation: NavigationProps[] = [
  { name: 'Account', href: '#', current: true },
  { name: 'Notifications', href: '#', current: false },
  { name: 'Billing', href: '#', current: false },
  { name: 'Teams', href: '#', current: false },
  { name: 'Integrations', href: '#', current: false },
]

type AppLayoutAccountProfile = {
  name?: string
  avatar?: string
}

type AppLayoutSidebarState = {
  sidebarOpen: boolean
  setSidebarOpen: Dispatch<SetStateAction<boolean>>
}

type AppLayoutProps = PropsWithChildren<
  AppLayoutAccountProfile & AppLayoutSidebarState
>

type LayoutContentProps = PropsWithChildren<{
  showSidebar: () => void
}>

type LayoutContentWithAsideProps = LayoutContentProps & {
  aside: ReactNode
}

type NavbarProps = {
  onClick: () => void
}

export function MobileNavbar({ onClick }: NavbarProps) {
  return (
    <div className="sticky top-0 z-40 flex items-center gap-x-6 bg-white px-4 py-4 shadow-sm sm:px-6 lg:hidden">
      <button
        type="button"
        className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
        onClick={onClick}
      >
        <span className="sr-only">Open sidebar</span>
        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
      </button>

      <div className="flex-1 text-sm font-semibold leading-6 text-gray-900">
        Metrics
      </div>
      <a href="#">
        <span className="sr-only">Your profile</span>
        <img
          className="h-8 w-8 rounded-full bg-gray-50"
          src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
          alt=""
        />
      </a>
    </div>
  )
}

export function DesktopAside({ children }: PropsWithChildren<{}>) {
  return (
    <aside className="fixed inset-y-0 left-72 hidden w-96 overflow-y-auto border-r border-gray-200 px-4 py-6 sm:px-6 lg:px-8 xl:block">
      {/* Secondary column (hidden on smaller screens) */}
      {children}
    </aside>
  )
}

export function SinglePane({ showSidebar, children }: LayoutContentProps) {
  return (
    <>
      <MobileNavbar onClick={showSidebar} />

      <div className="px-4 py-10 sm:px-6 lg:px-8 lg:py-6">{children}</div>
    </>
  )
}

export function DualPane({
  showSidebar,
  children,
  aside,
}: LayoutContentWithAsideProps) {
  return (
    <>
      <MobileNavbar onClick={showSidebar} />

      <div className="xl:pl-96">
        <div className="px-4 py-10 sm:px-6 lg:px-8 lg:py-6">{children}</div>
      </div>

      <DesktopAside>{aside}</DesktopAside>
    </>
  )
}

export default function AppLayout({
  children,
  name,
  avatar,
  sidebarOpen,
  setSidebarOpen,
}: AppLayoutProps) {
  // https://tailwindui.com/components/application-ui/page-examples/settings-screens#component-7c54612853cc6e70efb0474939579bac
  const theme = useContext(ThemeContext)

  return (
    <>
      <div>
        <Transition.Root show={sidebarOpen} as={Fragment}>
          <Dialog
            as="div"
            className="relative z-50 xl:hidden"
            onClose={setSidebarOpen}
          >
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className={`fixed inset-0 ${theme.backgroundModal}`} />
            </Transition.Child>

            <div className="fixed inset-0 flex">
              <Transition.Child
                as={Fragment}
                enter="transition ease-in-out duration-300 transform"
                enterFrom="-translate-x-full"
                enterTo="translate-x-0"
                leave="transition ease-in-out duration-300 transform"
                leaveFrom="translate-x-0"
                leaveTo="-translate-x-full"
              >
                <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                  <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in-out duration-300"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <div className="absolute left-full top-0 flex w-16 justify-center pt-5">
                      <button
                        type="button"
                        className="-m-2.5 p-2.5"
                        onClick={() => setSidebarOpen(false)}
                      >
                        <span className="sr-only">Close sidebar</span>
                        <XMarkIcon
                          className={`h-6 w-6 ${theme.textPrimary}`}
                          aria-hidden="true"
                        />
                      </button>
                    </div>
                  </Transition.Child>
                  {/* Sidebar component for mobile */}
                  <Sidebar
                    profile={{ name, avatar }}
                    navigation={defaultNavigation}
                    spaces={defaultTeams}
                  />
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>

        {/* Static sidebar for desktop */}
        <div className="hidden xl:fixed xl:inset-y-0 xl:z-50 xl:flex xl:w-72 xl:flex-col">
          <Sidebar
            profile={{ name, avatar }}
            navigation={defaultNavigation}
            spaces={defaultTeams}
          />
        </div>

        <div className="xl:pl-72">{children}</div>
      </div>
    </>
  )
}
