import { useContext } from 'react'

import { ThemeContext } from '@/components/ThemeContext'

import { Logo } from '@/components/Logo'

type SidebarNavigation = {
  name: string
  href: string
  icon: any
  current: boolean
}

type SidebarSpaces = {
  name: string
  href: string
  initial: string
  current: boolean
}

type SidebarProfile = {
  name?: string
  avatar?: string
}

type SidebarProps = {
  profile?: SidebarProfile
  navigation: SidebarNavigation[]
  spaces: SidebarSpaces[]
}

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export function Sidebar({ profile, navigation, spaces }: SidebarProps) {
  const theme = useContext(ThemeContext)

  return (
    <div
      className={`flex grow flex-col gap-y-5 overflow-y-auto px-6 ring-1 ${theme.sidebar}`}
    >
      <div className="flex h-16 shrink-0 items-center">
        <Logo className={`mx-auto h-8 w-8 ${theme.sidebarLogo}`} />
      </div>
      <nav className="flex flex-1 flex-col">
        <ul role="list" className="flex flex-1 flex-col gap-y-7">
          <li>
            <ul role="list" className="-mx-2 space-y-1">
              {navigation.map((item) => (
                <li key={item.name}>
                  <a
                    href={item.href}
                    className={classNames(
                      item.current ? theme.buttonPrimary : '',
                      'group flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6',
                    )}
                  >
                    <item.icon
                      className="h-6 w-6 shrink-0"
                      aria-hidden="true"
                    />
                    {item.name}
                  </a>
                </li>
              ))}
            </ul>
          </li>
          <li>
            <div className={`text-xs font-semibold leading-6`}>Your teams</div>
            <ul role="list" className="-mx-2 mt-2 space-y-1">
              {spaces.map((team) => (
                <li key={team.name}>
                  <a
                    href={team.href}
                    className={classNames(
                      team.current ? theme.buttonPrimary : '',
                      'group flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6',
                    )}
                  >
                    <span
                      className={`flex h-6 w-6 shrink-0 items-center justify-center rounded-lg border ${theme.sidebarIcon} text-[0.625rem] font-medium`}
                    >
                      {team.initial}
                    </span>
                    <span className="truncate">{team.name}</span>
                  </a>
                </li>
              ))}
            </ul>
          </li>
          <li className="-mx-6 mt-auto">
            <a
              href="#"
              className={`flex items-center gap-x-4 px-6 py-3 text-sm font-semibold leading-6`}
            >
              {profile?.avatar && (
                <img
                  className="h-8 w-8 rounded-full bg-gray-800"
                  src={profile.avatar}
                  alt={`Profile picture for ${profile?.name}`}
                />
              )}
              <span className="sr-only">Your profile</span>
              <span aria-hidden="true">{profile?.name || 'Agent 42'}</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  )
}
