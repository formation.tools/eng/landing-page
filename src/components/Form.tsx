import {
  Formik,
  FormikValues,
  FormikProps,
  Form as FormikForm,
  Field,
  FormikErrors,
} from 'formik'

import { ExclamationCircleIcon } from '@heroicons/react/20/solid'

import { WaitlistData } from '@/types/forms'

type ElementProps = {
  inputType: string
  name: string
  label: string
  placeholder?: string
  autocomplete?: string
  isRequired?: boolean
}

type FormInputProps = {
  label: string
  name: string
  inputType?: string
  placeholder?: string
  autocomplete?: string
  isValid: boolean
  isRequired?: boolean
  notice?: any
  disabled?: boolean
}

function FormInput({
  label,
  name,
  placeholder,
  autocomplete,
  isValid,
  isRequired,
  notice,
  disabled = false,
}: FormInputProps) {
  return (
    <div>
      <label htmlFor={name} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="relative mt-1 rounded-md shadow-sm">
        <Field
          name={name}
          placeholder={placeholder}
          type="string"
          autoComplete={autocomplete}
          required={isRequired}
          className={
            isValid
              ? 'block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm'
              : 'block w-full appearance-none rounded-md border border-red-300 px-3 py-2 pr-10 text-red-900 placeholder-red-300 focus:border-red-500 focus:outline-none focus:ring-red-500 sm:text-sm'
          }
          aria-invalid={isValid === false}
          aria-describedby={`${name}-error`}
        />
        {isValid === false && (
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
            <ExclamationCircleIcon
              className="h-5 w-5 text-red-500"
              aria-hidden="true"
            />
          </div>
        )}
      </div>
      {isValid === false && typeof notice === 'string' && (
        <p className="mt-2 text-sm text-red-600" id={`${name}-error`}>
          {notice}
        </p>
      )}
    </div>
  )
}

export function validateName(name?: string) {
  if (!name) {
    return { name: 'Please tell us how to address you.' }
  }

  return {}
}

export function validateEmail(email?: string) {
  if (!email) {
    return { email: 'We need an email address to reach you.' }
  }

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
    return { email: "The provided address doesn't work with our systems." }
  }

  return {}
}

// TODO: Migrate to separate file
const waitlistDefaults = {
  initialValues: { name: '', email: '', role: '' },
  validate: (values: WaitlistData) => {
    return {
      ...validateName(values.name),
      ...validateEmail(values.email),
    }
  },
  elements: [
    {
      inputType: 'string',
      label: 'What do we call you?',
      name: 'name',
      placeholder: 'Your name',
      autocomplete: 'name',
      isRequired: true,
    },
    {
      label: 'How can we reach you (by e-mail)?',
      name: 'email',
      inputType: 'string',
      placeholder: 'You email address',
      autocomplete: 'email',
      isRequired: true,
    },
    {
      label: 'Describe in 3 words (or more) what you do at work.',
      name: 'role',
      inputType: 'string',
      placeholder: 'Your role',
      autocomplete: 'organization-title',
    },
    {
      inputType: 'submit',
      label: 'Get listed!',
      name: 'submit',
    },
  ],
  elementsBuilder: (
    { errors, isSubmitting }: FormikProps<any>,
    elements: ElementProps[],
  ): JSX.Element[] =>
    elements.map((props) => {
      switch (props.inputType) {
        case 'submit':
          return (
            <div key="submit">
              <button
                type="submit"
                disabled={Object.keys(errors).length !== 0 || isSubmitting}
                className="flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              >
                {props.label}
              </button>
            </div>
          )
        default:
          return (
            <FormInput
              {...props}
              disabled={isSubmitting}
              key={props.name}
              isValid={errors.name === undefined}
              notice={errors.name}
            />
          )
      }
    }),
}

type FormBuilderProps = {
  elements: ElementProps[]
  elementsBuilder?: (
    { errors, isSubmitting },
    elements: ElementProps[],
  ) => JSX.Element[]
}

type FormDataProps<FormikValues> = {
  onSubmit: (data: FormikValues, done: () => void, success?: () => void) => void
  onSuccess?: () => void
  initialValues: FormikValues
  validate?: (values: FormikValues) => FormikErrors<FormikValues> | Promise<any>
}

export type FormProps = FormDataProps<FormikValues> &
  FormBuilderProps & {
    className?: string
  }

export function Form({
  onSubmit,
  onSuccess,
  className = 'space-y-6',
  initialValues,
  validate,
  elements,
  elementsBuilder,
}: FormProps) {
  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={(values, actions) => {
        onSubmit({ ...values }, () => actions.setSubmitting(false), onSuccess)
      }}
    >
      {({ errors, handleSubmit, isSubmitting }) => (
        <FormikForm
          className={className}
          onSubmit={(e) => {
            if (isSubmitting === true) {
              console.warn('Already submitting, so ignoring the new submission')
              return
            }
            handleSubmit(e)
          }}
        >
          {elementsBuilder &&
            elementsBuilder({ errors, isSubmitting }, elements)}

          {/* {process.env.NODE_ENV === 'development' && (
            <pre>
              keys: {Object.keys(errors).length}, isSubmitting:{' '}
              {isSubmitting.toString()}, disabled:{' '}
              {(Object.keys(errors).length !== 0 || isSubmitting).toString()}
            </pre>
          )} */}
        </FormikForm>
      )}
    </Formik>
  )
}

type WaitlistFormProps = FormDataProps<WaitlistData> & Partial<FormBuilderProps>

export function WaitlistForm({
  onSubmit,
  initialValues = waitlistDefaults.initialValues,
  validate = waitlistDefaults.validate,
  elements = waitlistDefaults.elements,
  elementsBuilder = waitlistDefaults.elementsBuilder,
}: WaitlistFormProps) {
  return Form({
    onSubmit,
    initialValues,
    validate,
    elements,
    elementsBuilder,
  })
}

export function SocialSigninButtonGroup() {
  return (
    <div className="mt-6">
      <div className="relative">
        <div className="absolute inset-0 flex items-center">
          <div className="w-full border-t border-gray-300" />
        </div>
        <div className="relative flex justify-center text-sm">
          <span className="bg-white px-2 text-gray-500">Or continue with</span>
        </div>
      </div>

      <div className="mt-6 grid grid-cols-3 gap-3">
        <div>
          <a
            href="#"
            className="inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
          >
            <span className="sr-only">Sign in with Facebook</span>
            <svg
              className="h-5 w-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
            >
              <path
                fillRule="evenodd"
                d="M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z"
                clipRule="evenodd"
              />
            </svg>
          </a>
        </div>

        <div>
          <a
            href="#"
            className="inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
          >
            <span className="sr-only">Sign in with Twitter</span>
            <svg
              className="h-5 w-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
            >
              <path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0020 3.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.073 4.073 0 01.8 7.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 010 16.407a11.616 11.616 0 006.29 1.84" />
            </svg>
          </a>
        </div>

        <div>
          <a
            href="#"
            className="inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
          >
            <span className="sr-only">Sign in with GitHub</span>
            <svg
              className="h-5 w-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
            >
              <path
                fillRule="evenodd"
                d="M10 0C4.477 0 0 4.484 0 10.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0110 4.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.203 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.942.359.31.678.921.678 1.856 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0020 10.017C20 4.484 15.522 0 10 0z"
                clipRule="evenodd"
              />
            </svg>
          </a>
        </div>
      </div>
    </div>
  )
}

type FormTypes =
  | 'registration'
  | 'platform-suggestion'
  | 'integration-suggestion'

export async function submit(input: FormikValues, kind: FormTypes) {
  const origin = window?.location?.href
  const body = JSON.stringify({ ...input, kind, origin })

  const response = await fetch('/api/waitlist', {
    method: 'POST',
    body,
  })

  const data = await response.json()

  if (response.status >= 200 && response.status < 300) {
    console.info(`Succeeded with submission: ${body}`)
    return data
  } else {
    throw new Error('Non-success response from server')
  }
}
