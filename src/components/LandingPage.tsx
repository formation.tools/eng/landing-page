import { FullScreenshotHero } from '@/components/Hero'
import HeroSimpleCentered from '@/components/HeroSimpleCentered'
import NavBar from '@/components/NavBar'
import { PricingNew } from '@/components/Pricing'
import FormSingleInputHorizontal from '@/components/InputForm'
import LogoCloudLightTextOnTopRow from '@/components/LogoCloudLightTextOnTopRow'
import { QuestionsNew } from '@/components/Questions'
import Legal from '@/components/Legal'
import FeatureWithProduct from '@/components/FeatureWithProduct'
import { Element } from '@/../types'
import { ThemeContext } from './ThemeContext'
import * as themes from '@/utils/theme'

import {
  WaitlistRegistrationFormData,
  IntegrationSuggestionFormData,
  PlatformSuggestionFormData,
} from '@/pages/index.page'

export interface LandingPageData {
  title: string
  slug: {
    current: string
    _type: string
  }
  themeName: 'lui' | 'basic'
  elements: Element[]
}

export interface LandingPageProps {
  landingPageData: LandingPageData
}

function LandingPage({ landingPageData }: LandingPageProps) {
  if (!landingPageData) {
    return <div>Loading</div>
  }

  const json = JSON.stringify(landingPageData)
  const { elements, themeName } = landingPageData

  const theme: themes.Theme =
    themeName === 'lui' ? themes.getLuiTheme() : themes.getBasicTheme()

  // Define the mapping between Sanity types and the components
  const componentMapping = {
    fullScreenshotHero: FullScreenshotHero,
    pricingNew: PricingNew,
    formSingleInputHorizontal: FormSingleInputHorizontal,
    questionsNew: QuestionsNew,
    logoCloudLightTextOnTopRow: LogoCloudLightTextOnTopRow,
    termsAndConditions: Legal,
    privacyPolicy: Legal,
    heroSimpleCentered: HeroSimpleCentered,
    navBar: NavBar,
    featureWithProduct: FeatureWithProduct,
  }

  return (
    <ThemeContext.Provider value={theme}>
      <main className={`${theme.backgroundPage}`}>
        <div>
          <div>
            {elements.map((element) => {
              const Component = componentMapping[element._type]

              if (!Component) {
                console.warn(`No component found for type "${element._type}"`)
                return null
              }

              // TODO:
              // Adjust props according to the component type
              let adjustedProps
              // TODO: add a runtime validation to validate the element prop at runtime
              switch (element._type) {
                case 'fullScreenshotHero':
                  adjustedProps = {
                    ...element,
                    screenshot: element.image,
                  }
                  break
                case 'heroSimpleCentered':
                  let adjustedNavigation
                  if (element.navBarData) {
                    adjustedNavigation =
                      element.navBarData.navigation?.map((navItem) => ({
                        name: navItem.linkName,
                        href: navItem.linkUrl,
                      })) || null
                  }

                  adjustedProps = {
                    ...element,
                    NavBarData: element.navBarData
                      ? {
                          ...element.navBarData,
                          navigation: adjustedNavigation,
                        }
                      : null,
                  }
                  break
                case 'heroSimpleCentered':
                  const { navigation = [], ...navBarData } =
                    element.navBarData || {}

                  adjustedProps = {
                    ...element,
                    navBarData: navBarData
                      ? { ...navBarData, navigation }
                      : navBarData,
                  }
                  break

                case 'pricingNew':
                  adjustedProps = element
                  break

                case 'formSingleInputHorizontal':
                  switch (element.formData) {
                    case 'WaitlistRegistrationFormData':
                      adjustedProps = {
                        ...WaitlistRegistrationFormData,
                        ...element,
                      }
                      break
                    case 'IntegrationSuggestionFormData':
                      adjustedProps = {
                        ...IntegrationSuggestionFormData,
                        ...element,
                      }
                      break
                    case 'PlatformSuggestionFormData':
                      adjustedProps = {
                        ...PlatformSuggestionFormData,
                        ...element,
                      }
                      break
                    default:
                      console.warn(
                        `Unsupported form data type "${element.formData}"`,
                      )
                      return null
                  }

                  break
                case 'questionsNew':
                  adjustedProps = element
                  break
                case 'logoCloudLightTextOnTopRow':
                  adjustedProps = {
                    headline: element.headline,
                    text: element.text,
                    CTA: element.CTA,
                    logos: element.logos,
                  }
                  break
                case 'featureWithProduct':
                  adjustedProps = {
                    ...element,
                  }
                  break
                case 'termsAndConditions':
                  adjustedProps = element
                  break
                case 'privacyPolicy':
                  adjustedProps = element
                  break
                default:
                  console.warn(
                    `No component found for type "${(element as any)._type}"`,
                  )
                  return <div>no component found</div>
              }

              return <Component {...adjustedProps} key={element._id} />
            })}
          </div>
        </div>
      </main>
    </ThemeContext.Provider>
  )
}

export default LandingPage
