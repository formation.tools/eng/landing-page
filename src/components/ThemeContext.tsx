import { createContext } from 'react'

import * as themes from '@/utils/theme'

export const ThemeContext = createContext(themes.getBasicTheme())
