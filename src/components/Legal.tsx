import React, { useContext } from 'react'
import { ThemeContext } from '@/components/ThemeContext'
import { PortableText } from '@portabletext/react'
import {
  PortableTextBlock,
  PortableTextMarkDefinition,
} from '@portabletext/types'
import ContentCentered from './ContentCentered'

interface LegalProps {
  title: string
  content: string
  portableTextContent: PortableTextBlock[]
}
interface LinkMark extends PortableTextMarkDefinition {
  _key: string
  _type: 'link'
  href: string
}
interface LinkProps {
  value?: LinkMark
  children: React.ReactNode
}

const myPortableTextComponents = {
  block: {
    h1: ({ children }: { children?: React.ReactNode }) => (
      <h1 className="text-5xl font-extrabold dark:text-white">{children}</h1>
    ),
    h2: ({ children }: { children?: React.ReactNode }) => (
      <h2 className="text-4xl font-bold dark:text-white">{children}</h2>
    ),
    h3: ({ children }: { children?: React.ReactNode }) => (
      <h3 className="text-3xl font-bold dark:text-white">{children}</h3>
    ),
    normal: ({ children }: { children?: React.ReactNode }) => (
      <p className="mb-4">{children}</p>
    ),
    listItem: ({ children }: any) => {
      return <li className="mb-1">{children}</li>
    },
  },

  list: {
    bullet: ({ children }: { children?: React.ReactNode }) => (
      <ul className="list-inside list-disc">{children}</ul>
    ),
    number: ({ children }: { children?: React.ReactNode }) => (
      <ol className="list-inside list-decimal">{children}</ol>
    ),
  },
  marks: {
    strong: ({ children }: { children: React.ReactNode }) => (
      <strong className="font-bold">{children}</strong>
    ),
    em: ({ children }: { children: React.ReactNode }) => (
      <em className="italic">{children}</em>
    ),
    link: ({ value, children }: LinkProps) => {
      const target = value?.href.startsWith('http') ? '_blank' : undefined
      return (
        <a
          href={value?.href ?? '#'}
          target={target}
          rel={target === '_blank' ? 'noopener noreferrer' : undefined}
          className="text-purple-500"
        >
          {children}
        </a>
      )
    },
  },
}

export const MyPortableText = ({ portableTextContent }) => {
  return (
    <PortableText
      value={portableTextContent}
      components={myPortableTextComponents}
    />
  )
}

const Legal: React.FC<LegalProps> = ({
  title,
  content,
  portableTextContent,
}) => {
  const theme = useContext(ThemeContext)

  return (
    <div className={`${theme.background}`}>
      <div className="mx-auto max-w-7xl px-6 py-24 sm:py-32 lg:px-8 lg:py-40">
        <div className={`mx-auto max-w-4xl ${theme.divideFaqs}`}>
          <h2
            className={`text-2xl font-bold leading-10 tracking-tight ${theme.textPrimary}`}
          >
            {title}
          </h2>
          <div className={`mt-6 ${theme.textSecondary}`}>
            <p className="text-base leading-7">{content}</p>
          </div>
          <MyPortableText portableTextContent={portableTextContent} />
        </div>
      </div>
      {/* <ContentCentered /> */}
    </div>
  )
}

export default Legal
