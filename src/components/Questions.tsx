import React, { useContext } from 'react'
import { ThemeContext } from '@/components/ThemeContext'

import { Disclosure } from '@headlessui/react'
import { MinusSmallIcon, PlusSmallIcon } from '@heroicons/react/24/outline'

type QuestionsProp = {
  questions: Array<{
    question: string
    answer: string
  }>
}

type DarkProps = {
  isDark: boolean
}

export function QuestionsNew({
  questions,
  isDark = false,
}: QuestionsProp & DarkProps) {
  isDark = true
  const theme = useContext(ThemeContext)

  return (
    <div className={`${theme.background}`}>
      <div className="mx-auto max-w-7xl px-6 py-24 sm:py-32 lg:py-40 lg:px-8">
        <div className={`mx-auto max-w-4xl divide-y ${theme.divideFaqs}`}>
          <h2
            className={`text-2xl font-bold leading-10 tracking-tight ${theme.textPrimary}`}
          >
            Frequently asked questions
          </h2>
          <dl className={`mt-10 space-y-6 divide-y ${theme.divideFaqs}`}>
            {questions.map((faq) => (
              <Disclosure as="div" key={faq.question} className="pt-6">
                {({ open }) => (
                  <>
                    <dt>
                      <Disclosure.Button
                        className={`flex w-full items-start justify-between text-left ${theme.textPrimary}`}
                      >
                        <span className="text-base font-semibold leading-7">
                          {faq.question}
                        </span>
                        <span className="ml-6 flex h-7 items-center">
                          {open ? (
                            <PlusSmallIcon
                              className="h-6 w-6"
                              aria-hidden="true"
                            />
                          ) : (
                            <MinusSmallIcon
                              className="h-6 w-6"
                              aria-hidden="true"
                            />
                          )}
                        </span>
                      </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as="dd" className="mt-2 pr-12">
                      <p
                        className={`text-base leading-7 ${theme.textSecondary}`}
                      >
                        {faq.answer}
                      </p>
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </div>
    </div>
  )
}
