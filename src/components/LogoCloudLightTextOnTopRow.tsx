import React, { useContext } from 'react'
import { ThemeContext } from './ThemeContext'
import Image from 'next/image'

type CTA = {
  buttonOne?: string
  buttonTwo?: string
}

type Logo = { asset: { url: string } } | JSX.Element

type LogoCloudWithTextProps = {
  headline: string
  text: string
  CTA?: CTA
  logos: Logo[]
}

// type LogoCloudWithTextProps = {
//   headline: string
//   text: string
//   CTA?: CTA
//   //   logos: JSX.Element[]
//   logos: { asset: { url: string } }[]
// }

export default function LogoCloudLightTextOnTopRow({
  headline,
  text,
  CTA,
  logos,
}: LogoCloudWithTextProps) {
  const theme = useContext(ThemeContext)
  theme.background = 'transparent'

  function isImageObject(logo: Logo): logo is { asset: { url: string } } {
    return (logo as any).asset !== undefined
  }

  return (
    <div className={`${theme.background} py-24 sm:py-32`}>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="grid grid-cols-1 items-center gap-x-8 gap-y-16 ">
          <div className="mx-auto w-full max-w-xl ">
            <h2
              className={`text-3xl font-bold tracking-tight ${theme.textPrimary}`}
            >
              {headline}
            </h2>
            <p className={`mt-6 text-lg leading-8 ${theme.textSecondary} `}>
              {text}
            </p>
            {CTA && (
              <div className="mt-8 flex flex-col items-center gap-x-6 gap-y-6 md:flex-row">
                <a
                  href="#"
                  className={`${theme.button} ${theme.buttonOutline} rounded-md px-3.5 py-2.5 text-sm font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2`}
                >
                  {CTA.buttonOne}
                </a>
                {CTA.buttonTwo && (
                  <a
                    href="#"
                    className={`cursor-not-allowed text-sm font-semibold ${theme.textPrimary}`}
                  >
                    {CTA.buttonTwo} <span aria-hidden="true">&rarr;</span>
                  </a>
                )}
              </div>
            )}
          </div>
          <div className="items-left mx-auto grid w-full max-w-xl grid-cols-2 items-center gap-x-10 gap-y-12 sm:gap-y-14 md:grid-cols-4 lg:mx-0 lg:max-w-none">
            {logos.map((logo, idx) => {
              //   if (React.isValidElement(logo)) {
              //     return React.cloneElement(logo, {
              //       key: idx,
              //       className: `max-h-16 ${logo.props.className}`,
              //     })
              if (React.isValidElement<HTMLDivElement>(logo)) {
                return (
                  <div key={idx} className={`max-h-16 ${logo.props.className}`}>
                    {logo}
                  </div>
                )
              } else if (isImageObject(logo) && logo.asset.url) {
                return (
                  <Image
                    key={idx}
                    src={logo.asset.url}
                    alt="Logo"
                    className={`max-h-16`}
                    width={100}
                    height={100}
                  />
                )
              } else {
                return null
              }
            })}
          </div>
        </div>
      </div>
    </div>
  )
}
