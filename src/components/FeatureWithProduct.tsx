import { useContext } from 'react'
import { ThemeContext } from './ThemeContext'

interface Feature {
  name: string
  description: string
  //   isWithIcon: boolean;
  icon: string
}

interface FeatureWithProductProps {
  features: Feature[]
  headline: string
  tagline: string
  text: string
  productShot: string
}

export default function FeatureWithProduct({
  features,
  headline,
  tagline,
  text,
  productShot,
}) {
  // TODO: @stefano use Provider to move theming from components -> pages
  const theme = useContext(ThemeContext)

  return (
    <div className={`overflow-hidden bg-transparent py-24 sm:py-32`}>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
          <div className="lg:pr-8 lg:pt-4">
            <div className="lg:max-w-lg">
              <h2
                className={`text-base font-semibold leading-7 ${theme.textAccent}`}
              >
                {tagline}
              </h2>
              <p
                className={`mt-2 text-3xl font-bold tracking-tight ${theme.textPrimary} sm:text-4xl`}
              >
                {headline}
              </p>
              <p className={`mt-6 text-lg leading-8 ${theme.textSecondary}`}>
                {text}
              </p>
              <dl
                className={`mt-10 max-w-xl space-y-8 text-base leading-7 ${theme.textSecondary} lg:max-w-none`}
              >
                {features.map((feature) => (
                  <div
                    key={feature.name}
                    // className={`{isWithIcon ? " pl-9" : "" }  relative `}
                    className={`${feature.icon ? 'pl-9' : ''} relative`}
                  >
                    <dt className={`inline text-xl ${theme.textSecondary}`}>
                      {/* {feature.isWithIcon && ( */}
                      {feature.icon && (
                        <feature.icon
                          className={`absolute left-1 top-1 h-5 w-5 ${theme.icon}`}
                          aria-hidden="true"
                        />
                      )}
                      {feature.name}
                    </dt>{' '}
                    <dd className="inline">{feature.description}</dd>
                  </div>
                ))}
              </dl>
            </div>
          </div>
          <div className="lg:pr-8 lg:pt-4">
            {productShot && (
              //   <ProductShot
              //     viewBox="0 0 436 366"
              //     className="h-full w-full object-contain"
              //   />
              <img
                src={productShot}
                alt="App screenshot"
                width={436}
                height={366}
                className="h-full w-full object-contain"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
