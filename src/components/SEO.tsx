import Head from 'next/head'
import config from '../config'

export type SeoProps = {
  siteTitle: string
  pageTitle: string
  description: string
  canonical: string
  ogUrl: string
  ogSiteName: string
  ogType: string
  ogTitle?: string
  ogDescription?: string
  ogImage: string
  twitterCard: string
  twitterTitle?: string
  twitterDescription?: string
  twitterImage: string
  twitterImageAlt: string
  twitterCreator?: string
  twitterSite: string
}

// TODO: Migrate to TypeScript
export default function SEO({
  pageTitle,
  description,
  canonical,
  ogTitle = pageTitle,
  ogType,
  ogDescription = description,
  ogImage,
  ogUrl,
  ogSiteName,
  twitterCard,
  twitterTitle = pageTitle,
  twitterDescription = description,
  twitterSite,
  twitterImage,
  twitterImageAlt,
  twitterCreator,
}: SeoProps) {
  const siteTitle = config.title

  return (
    <Head>
      {pageTitle && (
        <title>{`${pageTitle} ${siteTitle ? ` | ${siteTitle}` : ''}`}</title>
      )}
      {description && <meta name="description" content={description} />}
      {canonical && <link rel="canonical" href={canonical} />}
      {/* Open Graph Protocol tags */}
      {ogType && <meta property="og:type" content={ogType} />}
      {ogTitle && <meta property="og:title" content={ogTitle} />}
      {ogDescription && (
        <meta property="og:description" content={description} />
      )}
      {ogImage && <meta property="og:image" content={ogImage} />}
      {ogUrl && <meta property="og:url" content={config.siteUrl} />}
      {ogSiteName && <meta property="og:site_name" content={siteTitle} />}
      {/* Twitter Card tags */}
      {twitterCard && <meta name="twitter:card" content={twitterCard} />}
      {twitterTitle && <meta name="twitter:title" content={twitterTitle} />}
      {twitterDescription && (
        <meta name="twitter:description" content={twitterDescription} />
      )}
      {twitterSite && <meta name="twitter:site" content={twitterSite} />}
      {twitterImage && <meta name="twitter:image" content={twitterImage} />}
      {twitterImageAlt && (
        <meta name="twitter:image:alt" content={twitterImageAlt} />
      )}
      {twitterCreator && (
        <meta name="twitter:creator" content={config.social.twitter} />
      )}
    </Head>
  )
}

/* Possible values for twitter:card - The could be defined in the type of the property

Summary Card: This card includes a title, description, and thumbnail image. It's a good choice for most websites and blogs.

Summary Card with Large Image: This card is similar to the Summary Card, but features a larger image. It's a good choice if your content is visually-driven, such as a photo gallery or product page.

App Card: This card is designed for promoting mobile apps. It includes a title, description, and image, as well as a link to download the app.

Player Card: This card is designed for sharing audio or video content. It includes a title, description, thumbnail image, and a playable media player.

Gallery Card: This card is designed for sharing photo galleries. It includes a title, description, and multiple thumbnail images.

*/

/* twitter:title

Fallback values are og:title and then the content of the title tag

*/
