export function Logo(props) {
  return (
    <svg aria-hidden="true" viewBox="0 0 40 40" {...props}>
      <rect x="8" y="12" width="24" height="12" fill="current" />
      <rect x="20" y="12" width="12" height="24" fill="current" />
    </svg>
  )
}
