import { useState, useContext } from 'react'
import { ThemeContext } from '@/components/ThemeContext'

import { RadioGroup } from '@headlessui/react'
import { CheckIcon } from '@heroicons/react/20/solid'

import clsx from 'clsx'

import { Button } from '@/components/Button'
import { Container } from '@/components/Container'

function Plan({ name, price, description, href, features, featured = false }) {
  return (
    <section
      className={clsx(
        'flex flex-col rounded-3xl px-6 sm:px-8',
        'text-white',
        featured
          ? 'hover;opacity-100 order-first border-2 border-green-400 py-8 lg:order-none'
          : 'lg:py-8',
      )}
    >
      <h3 className="font-display mt-5 text-lg text-white">{name}</h3>
      <p
        className={clsx(
          'mt-2 text-base',
          featured ? 'text-green-400' : 'text-gray-400',
        )}
      >
        {description}
      </p>
      <p className="font-display order-first text-5xl font-light tracking-tight text-white">
        {price}
      </p>
      <ul
        role="list"
        className={clsx('order-last mt-10 flex flex-col gap-y-3 text-sm')}
      >
        {features.map((feature) => (
          <li key={feature} className="flex">
            <CheckIcon className="w-3.5" />
            <span className="ml-4">{feature}</span>
          </li>
        ))}
      </ul>
      <Button
        href={href}
        variant={featured ? 'solid' : 'outline'}
        color="white"
        className="mt-8"
        aria-label={`Get started with the ${name} plan for ${price}`}
      >
        Get started
      </Button>
    </section>
  )
}

export function Pricing() {
  return (
    <section
      id="pricing"
      aria-label="Pricing"
      className="bg-black py-20 sm:py-32"
    >
      <Container>
        <div className="md:text-center">
          <h2 className="font-display text-3xl tracking-tight text-white sm:text-4xl">
            <span className="relative whitespace-nowrap">
              <span className="relative">Simple pricing</span>
            </span>
          </h2>
          <p className="mt-4 text-lg">
            Flexible usage-based pricing to allow you to start small and adapt
            as needed.
          </p>
        </div>
        <div className="-mx-4 mt-16 grid max-w-2xl grid-cols-1 gap-y-10 sm:mx-auto lg:-mx-8 lg:max-w-none lg:grid-cols-3 xl:mx-0 xl:gap-x-8">
          <Plan
            name="cafe-mode"
            price="$0"
            description="Good for early founding teams still in the idea stage and with the guts to #buildinpublic."
            href="/waitlist"
            features={[
              '1✕ forge org (public repos only)',
              '1✕ IM scope (Slack org)',
              '1✕ file store (Dropbox or GDrive)',
              '5✕ team seats',
              '2✕ (temporary) guest seats',
              'Community Support',
            ]}
          />
          <Plan
            featured
            name="garage-mode"
            price="$5/seat"
            description="Good for teams that have grown beyond the founding team and have have some secret sauce to manage."
            href="/waitlist"
            features={[
              '1✕ forge scope (with private repos)',
              '1✕ IM scope (Slack org)',
              '1✕ file store (Dropbox or GDrive)',
              'Up to 40✕ seats',
              '5✕ (temporary) guest seats',
              'Within 24hrs Support',
              'Personal Inbox',
              'Cataloging Assistance (GPT-3 powered)',
              'Content Personalization (GPT-3 powered)',
              'SSO',
            ]}
          />
          <Plan
            name="office-mode"
            price="$10/seat"
            description="When things move fast and you need to check more boxes. Simplify work across more silos and larger teams"
            href="/waitlist"
            features={[
              '2✕ forge scope (GitLab or GitHub)',
              '5✕ IM scope (Slack org)',
              '5✕ file stores (Dropbox or GDrive)',
              'Up to 200✕ seats',
              '15✕ (temporary) guest seats',
              'Within 24hrs Support',
              'Personal Inbox',
              'Cataloging Assistence (GPT-3 powered)',
              'Content Personalization (GPT-3 powered)',
              'SSO',
              'Audit Logs',
            ]}
          />
        </div>
      </Container>
    </section>
  )
}

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

type PricingProps = {
  frequencies: Array<{ value: string; label: string; priceSuffix: string }>
  tiers: Array<{
    name: string
    id: string
    href: string
    price: { monthly: string; annualy: string }
    description: string
    features: Array<string>
    mostPopular: boolean
    mostPopularLabel?: string
  }>
}

export function PricingNew({ frequencies, tiers }: PricingProps) {
  const [frequency, setFrequency] = useState(frequencies[0])
  const theme = useContext(ThemeContext)

  return (
    <div className={`${theme.background} py-24 sm:py-32`}>
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-4xl text-center">
          <h2
            className={`text-lg font-semibold leading-8 tracking-tight ${theme.textAccent}`}
          >
            Pricing
          </h2>
          <p
            className={`mt-2 text-4xl font-bold tracking-tight ${theme.textPrimary} sm:text-5xl`}
          >
            Pricing plans for teams of&nbsp;all&nbsp;sizes
          </p>
        </div>
        <p
          className={`mx-auto mt-6 max-w-2xl text-center text-lg leading-8 ${theme.textSecondary}`}
        >
          Choose an affordable plan that’s packed with the best features for
          engaging your audience, creating customer loyalty, and driving sales.
        </p>
        <div className="mt-16 flex justify-center">
          <RadioGroup
            value={frequency}
            onChange={setFrequency}
            className={`grid grid-cols-2 gap-x-1 rounded-full p-1 text-center text-xs font-semibold leading-5 ${theme.buttonToggleRadioGroup}`}
          >
            <RadioGroup.Label className="sr-only">
              Payment frequency
            </RadioGroup.Label>
            {frequencies.map((option) => (
              <RadioGroup.Option
                key={option.value}
                value={option}
                className={({ checked }) =>
                  classNames(
                    checked
                      ? theme.buttonToggleActive
                      : theme.buttonToggleInactive,
                    'cursor-pointer rounded-full py-1 px-2.5',
                  )
                }
              >
                <span>{option.label}</span>
              </RadioGroup.Option>
            ))}
          </RadioGroup>
        </div>
        <div className="isolate mx-auto mt-10 grid max-w-md grid-cols-1 gap-8 lg:mx-0 lg:max-w-none lg:grid-cols-3">
          {tiers.map((tier) => (
            <div
              key={tier.id}
              className={classNames(
                tier.mostPopular
                  ? theme.pricingBlockPopular
                  : theme.pricingBlockRegular,
                'rounded-3xl p-8 xl:p-10',
              )}
            >
              <div className="flex items-center justify-between gap-x-4">
                <h3
                  // CHECK THIS
                  // The Dark version doesn't display the CSS classes dinamically based on the tier.mostPopular value
                  id={tier.id}
                  className={`text-lg font-semibold leading-8 ${
                    tier.mostPopular
                      ? theme.pricingBlockTitlePopular
                      : theme.pricingBlockTitleRegular
                  }`}
                >
                  {tier.name}
                </h3>
                {tier.mostPopular ? (
                  <p
                    className={`rounded-full py-1 px-2.5 text-xs font-semibold leading-5 ${theme.pricingBlockLabelStamp}`}
                  >
                    {tier.mostPopularLabel || 'Most popular'}
                  </p>
                ) : null}
              </div>
              <p className={`mt-4 text-sm leading-6 ${theme.textSecondary}`}>
                {tier.description}
              </p>
              <p className="mt-6 flex items-baseline gap-x-1">
                <span
                  className={`text-4xl font-bold tracking-tight ${theme.textPrimary}`}
                >
                  {tier.price[frequency.value]}
                </span>
                <span
                  className={`text-sm font-semibold leading-6 ${theme.textSecondary}`}
                >
                  {frequency.priceSuffix}
                </span>
              </p>
              <a
                href={tier.href}
                aria-describedby={tier.id}
                className={classNames(
                  tier.mostPopular
                    ? theme.pricingButtonPopular
                    : theme.pricingButtonRegular,
                  '${theme.buttonOutline} mt-6 block rounded-md py-2 px-3 text-center text-sm font-semibold leading-6',
                )}
              >
                Buy plan
              </a>
              <ul
                role="list"
                className={`mt-8 space-y-3 text-sm leading-6 ${theme.textSecondary} xl:mt-10`}
              >
                {tier.features.map((feature) => (
                  <li key={feature} className="flex gap-x-3">
                    <CheckIcon
                      className={`h-6 w-5 flex-none ${theme.pricingBlockTextPrimary}`}
                      aria-hidden="true"
                    />
                    {feature}
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
