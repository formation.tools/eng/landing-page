import { useContext } from 'react'
import { ThemeContext } from './ThemeContext'

import Image from 'next/image'
import { Button } from '@/components/Button'
import { Container } from '@/components/Container'

import Typewriter from 'typewriter-effect'

export function Hero() {
  const theme = useContext(ThemeContext)

  return (
    <Container className="pt-20 pb-16 text-center lg:pt-32">
      <div
        className={`font-display mx-auto max-w-4xl text-5xl font-medium tracking-tight ${theme.textPrimary} sm:text-7xl`}
      >
        <h1>Intel & progress tracking for</h1>{' '}
        <div className={`mt-3 rounded-md bg-black p-2 ${theme.textAccent}`}>
          <Typewriter
            options={{
              cursor: '_',
              strings: [
                'devs',
                'eng 🔧',
                'science 🧑‍🔬',
                'makers 👷🏽‍♂️',
                'hackers 👨🏿‍💻',
                'research 🧪',
                'magicians 🪄',
                'rocketeers 🚀',
                'teams',
              ],
              delay: 60,
              autoStart: true,
              loop: true,
            }}
          />
        </div>
      </div>
      <div></div>
      <p
        className={`font-display mx-auto mt-6 max-w-2xl text-lg tracking-tight ${theme.textPrimary}`}
      >
        Fully <span className="font-black">API</span>-controllable{' '}
        <span className="font-black">git</span>-tracked &amp;{' '}
        <span className="font-black">FLOSS</span>-y{' '}
        <em>tools for collaborative thought</em> for teams focussed on shipping!
      </p>
      <div className="mt-10 flex justify-center gap-x-6">
        <Button color="black" href="/waitlist" className="font-display">
          Get on the waitlist
        </Button>
      </div>
    </Container>
  )
}

type FullScreenshotHeroProps = {
  title: string
  text: string
  labelCallToAction: string
  labelMore: string
  //   screenshot: StaticImageData
  screenshot: string
}

export function FullScreenshotHero(p: FullScreenshotHeroProps) {
  const theme = useContext(ThemeContext)

  return (
    <div className={`isolate ${theme.background}`}>
      <main>
        <div className="relative py-24 sm:py-32 lg:pb-40">
          <div className="mx-auto max-w-7xl px-6 lg:px-8">
            <div className="mx-auto max-w-2xl text-center">
              <h1
                className={`text-4xl font-bold tracking-tight ${theme.textPrimary} sm:text-6xl`}
              >
                {p.title}
              </h1>
              <p className={`mt-6 text-lg leading-8 ${theme.textSecondary}`}>
                {p.text}
              </p>
              <div className="mt-10 flex items-center justify-center gap-x-6">
                <a
                  href="#"
                  className={`rounded-md px-3.5 py-1.5 text-base font-semibold leading-7 shadow-sm ${theme.button} focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ${theme.buttonOutline}`}
                >
                  {p.labelCallToAction}
                </a>
                <a
                  href="#"
                  className={`text-base font-semibold leading-7 ${theme.textPrimary}`}
                >
                  {p.labelMore}
                  <span aria-hidden="true">&rarr;</span>
                </a>
              </div>
            </div>
            <div className="mt-16 flow-root sm:mt-24">
              {/* FIXME: Correct as dark layout seems to lack this */}
              <div className="-m-2 rounded-xl bg-gray-900/5 p-2 ring-1 ring-inset ring-gray-900/10 lg:-m-4 lg:rounded-2xl lg:p-4">
                <Image
                  src={p.screenshot}
                  alt="App screenshot"
                  width={2432}
                  height={1442}
                  className="rounded-md shadow-2xl ring-1 ring-gray-900/10"
                />
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}
