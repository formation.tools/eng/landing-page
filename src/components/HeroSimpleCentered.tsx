import { useContext } from 'react'
import { ThemeContext } from './ThemeContext'

import NavBar from './NavBar'
import { NavBarDataProps } from './NavBar'

type HeroSimpleCenteredDataProps = {
  companyName: string
  headline: string
  NavBarData?: NavBarDataProps
}

export default function HeroSimpleCentered({
  headline,
  NavBarData,
}: HeroSimpleCenteredDataProps) {
  const theme = useContext(ThemeContext)

  return (
    <div className={`${theme.background} `}>
      {NavBarData && <NavBar {...NavBarData} />}
      <div className="relative isolate px-6 pt-14 lg:px-8">
        <div className="mx-auto max-w-2xl py-12 sm:py-12 lg:py-24">
          <div className="text-center">
            <h1
              className={`text-4xl font-bold tracking-tight ${theme.textPrimary} sm:text-6xl`}
            >
              {headline}
            </h1>
          </div>
        </div>
      </div>
    </div>
  )
}
