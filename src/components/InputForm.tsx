/*
https://tailwindui.com/components/marketing/sections/newsletter-sections#component-2da53d5b251438c875c9d2fec8b3d3c0
*/

import { Fragment, useContext, useState } from 'react'
import { Field } from 'formik'

import { ThemeContext } from './ThemeContext'

import { Form, FormProps } from '@/components/Form'

type HeadlineProps = {
  title: string
  subtitle?: string
  thanksLabel: string
  tryAgainLabel: string
}

export type InputFormProps = HeadlineProps & {
  form: FormProps
}

export default function FormSingleInputHorizontal({
  title,
  subtitle,
  thanksLabel,
  tryAgainLabel,
  form,
}: InputFormProps) {
  const theme = useContext(ThemeContext)

  const [isFormVisible, setIsFormVisible] = useState(true)

  return (
    <div className={`py-16 sm:py-24 lg:py-32`}>
      <div className="mx-auto my-auto block max-w-7xl px-6 lg:px-8">
        <div
          className={`w-full text-3xl tracking-tight ${theme.textPrimary} sm:text-4xl`}
        >
          <h2 className="my-auto block text-center font-bold sm:block">
            {title}
          </h2>
          {subtitle && (
            <p className="my-auto block pt-8 text-center text-xl">{subtitle}</p>
          )}
        </div>
        <div className="my-auto flex w-full justify-center">
          <div className="my-auto mt-10 block">
            {isFormVisible && (
              <Form
                initialValues={form.initialValues}
                validate={form.validate}
                onSuccess={() => setIsFormVisible(false)}
                onSubmit={form.onSubmit}
                className="mx-auto grid gap-x-2 gap-y-2 md:mx-0 md:flex md:w-min md:w-full"
                elements={form.elements}
                elementsBuilder={({ errors, isSubmitting }, elements) => {
                  return elements.map((props) => {
                    switch (props.inputType) {
                      case 'submit':
                        return (
                          <button
                            key={props.name}
                            type="submit"
                            disabled={
                              Object.keys(errors).length !== 0 || isSubmitting
                            }
                            className={`flex-auto rounded-md md:flex-none ${theme.button} mt-3 w-full py-2.5 px-3.5 text-sm font-semibold focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 md:mt-0 md:max-w-fit`}
                          >
                            {props.label}
                          </button>
                        )
                      default:
                        return (
                          <Fragment key={props.name}>
                            <label htmlFor={props.name} className="sr-only">
                              {props.label}
                            </label>
                            <Field
                              disabled={isSubmitting}
                              name={props.name}
                              type={props.inputType}
                              required={props.isRequired}
                              className={`min-w-0 flex-auto rounded-md border-0 bg-white px-3.5 py-2 md:mr-4 ${theme.textPrimary} ${theme.input} shadow-sm ring-1 ring-inset focus:ring-2 focus:ring-inset sm:leading-6 md:mr-0 md:text-sm`}
                              placeholder={props.placeholder}
                              aria-invalid={errors[props.name] !== undefined}
                              autoComplete={props.autocomplete}
                            />
                          </Fragment>
                        )
                    }
                  })
                }}
              />
            )}

            {!isFormVisible && (
              <div
                className={`text-md justify-center text-center font-bold tracking-tight ${theme.textPrimary} sm:text-md`}
              >
                <p>
                  {thanksLabel}
                  <button
                    onClick={() => setIsFormVisible(true)}
                    className={`w-full flex-none rounded-md md:max-w-fit ${theme.button} ml-3 py-2.5 text-sm font-semibold focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 md:px-3.5`}
                  >
                    {tryAgainLabel}
                  </button>
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
