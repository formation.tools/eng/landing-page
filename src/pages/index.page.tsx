import { useState } from 'react'

import { ThemeContext } from '@/components/ThemeContext'
import * as themes from '@/utils/theme'

import { Modal } from '@/components/Modal'
import { WaitlistForm, validateEmail, submit } from '@/components/Form'

import HeroSimpleCentered from '@/components/HeroSimpleCentered'
import FormSingleInputHorizontal, {
  InputFormProps,
} from '@/components/InputForm'
import FeatureWithProduct from '@/components/FeatureWithProduct'
import { LargeScreenshotFeatures } from '@/components/PrimaryFeatures'
import SEO, { SeoProps } from '@/components/SEO'
import config from '@/config'

const theme = themes.getLuiTheme()

import ogImageTwitter from '/public/images/og-image-600x315.png'
import ogImage from '/public/images/twitter-card-recommended-600x335.png'

import NotionSVG from '/public/logos/new/Notion.svg'
import ConfluenceSVG from '/public/logos/new/Confluence.svg'
import Microsoft365SVG from '/public/logos/new/Microsoft_365.svg'
import GoogleSuiteSVG from '/public/logos/new/GoogleSuite.svg'
import SlackSVG from '/public/logos/new/Slack.svg'
import MicrosoftTeamsSVG from '/public/logos/new/MicrosoftTeams.svg'
import GitHubSVG from '/public/logos/new/GitHub.svg'
import GitLabSVG from '/public/logos/new/GitLab.svg'
import ReadtheDocsSVG from '/public/logos/new/ReadTheDocs.svg'

import DiagSVG2 from '/public/ft-diagram2.svg'
import ScreenFindSVG from '/public/ftbot-find.svg'

import { CloudArrowUpIcon, LockClosedIcon } from '@heroicons/react/20/solid'
import LogoCloudLightTextOnTopRow from '@/components/LogoCloudLightTextOnTopRow'

/* SEO Props */

// TODO: This must be in SEO (which must be converted to TS)

const sampleSEOProps: SeoProps = {
  siteTitle: config.title,
  pageTitle: 'Search all your knowledge from a single place',
  description:
    'AI-powered capability made simple! Use AI across the tools where you work such as your Google Drive, your GitHub, your Slack and more. Implement AI-capability for your end-users across the tools where they work by using simple APIs.',
  canonical: 'https://fortoo.io/',
  /* og */
  ogUrl: 'https://fortoo.io/',
  ogSiteName: '',
  ogType: 'website',
  ogDescription: 'Get work done with AI, without the hassle!',
  ogImage: ogImage.src,
  /* twitter */
  // if  images are stored on another server
  // ogImage: `${config.siteUrl}/images/og-image.jpg`,
  twitterCard: 'summary',
  twitterDescription:
    "Use AI to simplify work by helping you find information or spot conflicts. Build AI apps by providing similar capabilities on top of your user's data without having to fiddle with the details.",
  twitterImage: ogImageTwitter.src,
  twitterImageAlt:
    'Julia asks for the meeting notes where we decided who owns what deliverables for the board meeting next week and the model starts drafting a quick answer.',
  twitterSite: config.social.twitter,
}

/* Hero Section */

const HeroSimpleCenteredData = {
  companyName: 'fortoo',
  headline: '/fortoo',
}

export const NavBarData = {
  navigation: [
    { name: 'Product', href: '#' },
    { name: 'Features', href: '#' },
    { name: 'Marketplace', href: '#' },
    { name: 'Company', href: '#' },
  ],
  companyName: 'fortoo',
  logo: '/logos/Tailwind_CSS_Logo.svg',
}

export const WaitlistRegistrationFormData: InputFormProps = {
  title: 'Search all your knowledge from a single place',
  thanksLabel: 'Thanks! You are waitlisted',
  tryAgainLabel: 'Register another email?',
  form: {
    initialValues: { email: '', name: 'Unnamed' },
    validate: (values) => {
      const report = {
        ...validateEmail(values.email),
      }
      return report
    },
    onSubmit: async (data, done, success) => {
      try {
        const response = await submit(data, 'registration')
        success && success()
      } catch (error) {
        console.error('Failure to submit waitlist registration.')
      }
      done()
    },
    elements: [
      //{
      //  label: 'Name',
      //  name: 'name',
      //  placeholder: 'Beeblebrox',
      //  inputType: 'string',
      //  isRequired: true,
      //},
      {
        label: 'Email',
        name: 'email',
        placeholder: 'you@example.com',
        inputType: 'email',
        isRequired: true,
        autocomplete: 'email',
      },
      {
        label: 'Give me early access!',
        name: 'submit',
        inputType: 'submit',
      },
    ],
  },
}

/* Feature I Section */

const FeatureFirstData = {
  features: [
    {
      name: 'Is this documented somewhere? Where?',
      description: '',
    },
    {
      name: 'Your company stores information in many places. Why not search all of them at once from your messenger?',
      description: '',
    },
    {
      name: 'Just ask our AI Slack bot to find what you need.',
      description: '',
    },
    {
      name: 'Our bot will also be available for Microsoft Teams and other communications platforms soon!',
      description: '',
    },
  ],
  headline: 'Find anything you need from a single place.',
  description: 'Search directly from your messenger app.',
  tagline: '',
  text: '',
}

export const PlatformSuggestionFormData: InputFormProps = {
  title: 'Need it for a different communication platform? Let us know:',
  thanksLabel: 'Thanks for your input!',
  tryAgainLabel: 'Suggest another platform?',
  form: {
    initialValues: { platform: '', email: '' },
    validate: (values) => {
      const report = {
        ...validateEmail(values.email),
        ...(values?.platform?.length > 2
          ? {}
          : { platform: 'Platform must be specified' }),
      }
      return report
    },
    onSubmit: async (data, done, success) => {
      try {
        const response = await submit(data, 'platform-suggestion')
        success && success()
      } catch (error) {
        console.error('Failure to submit platform suggestion.')
      }
      done()
    },
    elements: [
      {
        label: 'Platform',
        name: 'platform',
        placeholder: 'Platform name',
        inputType: 'string',
        isRequired: true,
      },
      {
        label: 'Email',
        name: 'email',
        placeholder: 'you@example.com',
        inputType: 'email',
        isRequired: true,
        autocomplete: 'email',
      },
      {
        label: 'Bring your bot there!',
        name: 'submit',
        inputType: 'submit',
      },
    ],
  },
}
/* Feature II Section */

const FeatureSecondData = {
  features: [
    {
      name: "You know that keyword-based search isn't good enough",
      description: '',
      icon: CloudArrowUpIcon,
    },
    {
      name: 'Our semantic search engine is powered by the latest AI language models for the best search results.',
      description: '',
      icon: LockClosedIcon,
    },
    // {
    //   name: 'Database backups.',
    //   description:
    //     'Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.',
    //   icon: ServerIcon,
    // },
  ],
  headline: "Find what you're really looking for. With AI.",
  tagline: '',
  text: '',
}

const diagramData = {
  features: [
    {
      name: 'Easy access',
      description:
        'Search directly from Slack, our web app or just use our API. We sort out the plumbing so you can focus on work.',
    },
    {
      name: 'Current data',
      description:
        'Use AI over your current data, wherever it is. Stop worrying about imports and integrations.',
    },
    {
      name: 'AI Actions',
      description:
        'Focus on useful actions like semantic search, conflict detection and more and forget about prompting and models details.',
    },
  ],
  isFeaturedInline: false,
  punchline: "Find what you're really looking for. With AI.",
  title: '',
  text: '',
}

/* Integrations */

const classNameImg = 'max-h-13  object-scale-down object-left lg:object-center'
const LogoCloudLightData = {
  headline: 'Search everywhere.',
  text: "We're working hard to integrate all the services and tools that are relevant to your business.",
  CTA: {
    buttonOne: 'Create account',
    buttonTwo: 'Contact us',
  },
  logos: [
    <NotionSVG key="notion" className={classNameImg} />,
    <ConfluenceSVG key="confluence" className={classNameImg} />,
    <Microsoft365SVG key="ms365" className={classNameImg} />,
    <GoogleSuiteSVG key="gworkspaces" className={classNameImg} />,
    <SlackSVG key="slack" className={classNameImg} />,
    <MicrosoftTeamsSVG key="msteams" className={classNameImg} />,
    <GitHubSVG key="github" className={classNameImg} />,
    <GitLabSVG key="gitlab" className={classNameImg} />,
    <ReadtheDocsSVG key="readthedocs" className={classNameImg} />,
  ],
}

export const IntegrationSuggestionFormData: InputFormProps = {
  title: 'What integrations do you need? Let us know:',
  thanksLabel: 'Thanks for your input!',
  tryAgainLabel: 'Suggest another integration?',
  form: {
    initialValues: { integration: '', email: '' },
    validate: (values) => {
      const report = {
        ...validateEmail(values.email),
        ...(values?.integration?.length > 2
          ? {}
          : { integration: 'Integration must be specified' }),
      }
      return report
    },
    onSubmit: async (data, done, success) => {
      try {
        const response = await submit(data, 'integration-suggestion')
        success && success()
      } catch (error) {
        console.error('Failure to submit integration suggestion.')
      }
      done()
    },
    elements: [
      {
        label: 'Integration',
        name: 'integration',
        placeholder: 'Integration name',
        inputType: 'string',
        isRequired: true,
      },
      {
        label: 'Email',
        name: 'email',
        placeholder: 'you@example.com',
        inputType: 'email',
        isRequired: true,
        autocomplete: 'email',
      },
      {
        label: 'Build this integration!',
        name: 'submit',
        inputType: 'submit',
      },
    ],
  },
}

const signupData = {
  title: "Let's get you on the waitlist!",
}

export default function Home() {
  const [isOpen, setIsOpen] = useState(true)

  return (
    <>
      <SEO {...sampleSEOProps} />
      {false && (
        <Modal
          {...signupData}
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          content={
            <WaitlistForm
              initialValues={{ name: '', email: '' }}
              onSubmit={async (data, unlockForm) => {
                try {
                  const response = await submit(data, 'registration')

                  if (response.error === undefined) {
                    // TODO: Close modal upon completion of happy path
                    setIsOpen(false)
                  } else {
                    console.error(
                      'Submission not registered, probably due to a formatting issue',
                    )
                    // TODO: Report issue in modal
                  }
                } catch (error) {
                  console.error(
                    'Well, that failed spectacularly. It must be us.',
                  )
                  console.error(error)
                }
                // Arm form for potentially another try
                unlockForm()
              }}
            />
          }
        />
      )}
      <ThemeContext.Provider value={theme}>
        <main className={`${theme.backgroundPage}`}>
          <section>
            <HeroSimpleCentered
              {...HeroSimpleCenteredData}
              //   NavBarData={NavBarData}
            />
            <FormSingleInputHorizontal {...WaitlistRegistrationFormData} />
          </section>
          <section>
            <FeatureWithProduct
              {...FeatureFirstData}
              productShot={ScreenFindSVG}
            />
            <FormSingleInputHorizontal {...PlatformSuggestionFormData} />
            <section>
              <LargeScreenshotFeatures
                {...diagramData}
                screenshot={DiagSVG2}
                isWithGradient={false}
              />
            </section>
          </section>
          <section>
            <LogoCloudLightTextOnTopRow {...LogoCloudLightData} />
            <FormSingleInputHorizontal {...IntegrationSuggestionFormData} />
          </section>
          <footer className="h-32 w-full"></footer>
        </main>
      </ThemeContext.Provider>
    </>
  )
}
