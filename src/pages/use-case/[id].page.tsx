import {
  GetStaticPropsResult,
  GetStaticPathsResult,
  GetStaticPropsContext,
} from 'next'

import {
  fullScreenshotHeroQuery,
  pricingNewQuery,
  logoCloudLightTextOnTopRowQuery,
  formSingleInputHorizontalQuery,
  questionsNewQuery,
  termsAndConditionsQuery,
  privacyPolicyQuery,
  heroSimpleCenteredQuery,
  featureWithProductQuery,
} from '../../utils/GROQueries'

import LandingPage, {
  LandingPageProps,
  LandingPageData,
} from '../../components/LandingPage'

import { fetchPageData } from '../../utils/GROQueries'
import { client } from '../../../sanity/lib/client'

interface UseCaseProps {
  useCaseData: LandingPageData
}

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  try {
    const landingPages = await client.fetch<LandingPageData[]>(
      '*[_type == "landingPage"]',
    )
    const paths = landingPages.map((landingPage) => ({
      params: { id: landingPage.slug.current },
    }))

    return {
      paths,
      fallback: false,
    }
  } catch (error) {
    console.error(error)
    return {
      paths: [],
      fallback: false,
    }
  }
}

export async function getStaticProps({
  params,
}: GetStaticPropsContext): Promise<GetStaticPropsResult<UseCaseProps>> {
  const id = params?.id as string | undefined
  if (!id) {
    return {
      notFound: true,
    }
  }

  const useCaseData = await fetchPageData(id)

  if (!useCaseData) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      useCaseData,
    },
  }
}

interface UseCaseData extends LandingPageData {}
interface UseCaseProps {
  useCaseData: UseCaseData
}

export default function UseCasePage({ useCaseData }: UseCaseProps) {
  return <LandingPage landingPageData={useCaseData} />
}
