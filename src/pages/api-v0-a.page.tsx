import { useState } from 'react'
import { ThemeContext } from '@/components/ThemeContext'

import * as themes from '@/utils/theme'
import { Modal } from '@/components/Modal'

import {
  WaitlistData,
  PlatformSuggestionData,
  IntegrationSuggestionData,
} from '@/types/forms'

import HeroSimpleCentered from '@/components/HeroSimpleCentered'
import FeatureWithProduct from '@/components/FeatureWithProduct'
import { LargeScreenshotFeatures } from '@/components/PrimaryFeatures'
import SEO, { SeoProps } from '@/components/SEO'
import config from '@/config'

async function submit<T>(input: T, kind) {
  const origin = window?.location?.href
  const body = JSON.stringify({ ...input, kind, origin })

  const response = await fetch('/api/waitlist', {
    method: 'POST',
    body,
  })

  const data = await response.json()

  if (response.status >= 200 && response.status < 300) {
    console.info(`Succeeded with submission: ${body}`)
    return data
  } else {
    throw new Error('Non-success response from server')
  }
}

async function submitWaitlistRegistration(submission: WaitlistData) {
  return submit<WaitlistData>(submission, 'registration')
}

async function submitPlatformSuggestion(suggestion: PlatformSuggestionData) {
  return submit<PlatformSuggestionData>(suggestion, 'platform-suggestion')
}

import ogImageTwitter from '/public/images/og-image-600x315.png'
import ogImage from '/public/images/twitter-card-recommended-600x335.png'

async function submitIntegrationSuggestion(
  suggestion: IntegrationSuggestionData,
) {
  return submit<IntegrationSuggestionData>(suggestion, 'integration-suggestion')
}

//import NotionSVG from '/public/logos/new/Notion.svg'
import NotionSVG from '/public/logos/new/Notion.svg'
import ConfluenceSVG from '/public/logos/new/Confluence.svg'
import Microsoft365SVG from '/public/logos/new/Microsoft_365.svg'
import GoogleSuiteSVG from '/public/logos/new/GoogleSuite.svg'
import SlackSVG from '/public/logos/new/Slack.svg'
import MicrosoftTeamsSVG from '/public/logos/new/MicrosoftTeams.svg'
import GitHubSVG from '/public/logos/new/GitHub.svg'
import GitLabSVG from '/public/logos/new/GitLab.svg'
import ReadtheDocsSVG from '/public/logos/new/ReadTheDocs.svg'

import DiagSVG from '/public/ft-diagram.svg'
import DiagSVG2 from '/public/ft-diagram2.svg'
import ScreenFindSVG from '/public/ftbot-find.svg'

import { CloudArrowUpIcon, LockClosedIcon } from '@heroicons/react/20/solid'
import LogoCloudLightTextOnTopRow from '@/components/LogoCloudLightTextOnTopRow'

/* SEO Props */

// TODO: This must be in SEO (which must be converted to TS)

const sampleSEOProps: SeoProps = {
  siteTitle: config.title,
  pageTitle: 'Safely use AI with your evolving data',
  description:
    'AI-powered capability made simple! Use AI across the tools where work happens such as your Google Drive, Notion, Email, your Slack and more.',
  canonical: 'https://fortoo.ai/',
  /* og */
  ogUrl: 'https://fortoo.ai/',
  ogSiteName: '',
  ogType: 'website',
  // I feel a better solution is possible for ogTitle and twitterTitle
  //ogTitle: () => this.pageTitle,
  ogDescription: 'Safely use AI, without the hassle!',
  ogImage: ogImage.src,
  /* twitter */
  // if  images are stored on another server
  // ogImage: `${config.siteUrl}/images/og-image.jpg`,
  twitterCard: 'summary',

  //twitterTitle: () => this.pageTitle,
  twitterDescription:
    "Use AI to simplify work by helping you find information or spot conflicts. Build AI apps by providing similar capabilities on top of your user's data without having to fiddle with the details.",
  twitterImage: ogImageTwitter.src,
  twitterImageAlt:
    'Julia asks for the meeting notes where we decided who owns what deliverables for the board meeting next week and the model starts drafting a quick answer.',
  twitterSite: config.social.twitter,
}

/* Hero Section */

const HeroSimpleCenteredData = {
  companyName: 'fortoo',
  headline: 'fortoo',
}

const NavBarData = {
  navigation: [
    { name: 'Product', href: '#' },
    { name: 'Features', href: '#' },
    { name: 'Marketplace', href: '#' },
    { name: 'Company', href: '#' },
  ],
  companyName: 'fortoo',
  logo: '/logos/Tailwind_CSS_Logo.svg',
}

/* Feature I Section */

const FeatureFirstData = {
  features: [
    {
      name: 'Is this documented somewhere? Where?',
      description: '',
    },
    {
      name: 'Your company stores information in many places. Why not search all of them at once from your messenger?',
      description: '',
    },
    {
      name: 'Just ask our AI Slack bot to find what you need.',
      description: '',
    },
    {
      name: 'Our bot will also be available for Microsoft Teams and other communications platforms soon!',
      description: '',
    },
  ],
  headline: 'Find anything you need from a single place.',
  description: 'Search directly from your messenger app.',
  tagline: '',
  text: '',
}

const FeatureSecondData = {
  features: [
    {
      name: "You know that keyword-based search isn't good enough",
      description: '',
      icon: CloudArrowUpIcon,
    },
    {
      name: 'Our semantic search engine is powered by the latest AI language models for the best search results.',
      description: '',
      icon: LockClosedIcon,
    },
    // {
    //   name: 'Database backups.',
    //   description:
    //     'Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.',
    //   icon: ServerIcon,
    // },
  ],
  headline: "Find what you're really looking for. With AI.",
  tagline: '',
  text: '',
}

const diagramData = {
  features: [
    {
      name: 'Easy access',
      description:
        'Search directly from Slack, our web app or just use our API. We sort out the plumbing so you can focus on work.',
    },
    {
      name: 'Current data',
      description:
        'Use AI over your current data, wherever it is. Stop worrying about imports and integrations.',
    },
    {
      name: 'AI Actions',
      description:
        'Focus on useful actions like semantic search, conflict detection and more and forget about prompting and models details.',
    },
  ],
  isFeaturedInline: false,
  punchline: "Find what you're really looking for. With AI.",
  title: '',
  text: '',
}

/* Integrations */

const classNameImg = 'max-h-13  object-scale-down object-left lg:object-center'
const LogoCloudLightData = {
  headline: 'Search everywhere.',
  text: "We're working hard to integrate all the services and tools that are relevant to your business.",
  CTA: {
    buttonOne: 'Create account',
    //buttonTwo: 'Some of our current integrations',
  },
  logos: [
    <NotionSVG key="notion" className={classNameImg} />,
    <ConfluenceSVG key="confluence" className={classNameImg} />,
    <Microsoft365SVG key="ms365" className={classNameImg} />,
    <GoogleSuiteSVG key="gworkspaces" className={classNameImg} />,
    <SlackSVG key="slack" className={classNameImg} />,
    <MicrosoftTeamsSVG key="msteams" className={classNameImg} />,
    <GitHubSVG key="github" className={classNameImg} />,
    <GitLabSVG key="gitlab" className={classNameImg} />,
    <ReadtheDocsSVG key="readthedocs" className={classNameImg} />,
  ],
}

const signupData = {
  title: "Let's get you on the waitlist!",
}

export default function APILanding() {
  const [isDark, SetIsDark] = useState(true)
  const [isOpen, setIsOpen] = useState(true)
  const theme = themes.getBasicTheme()

  return (
    <>
      <SEO {...sampleSEOProps} />
      {false && (
        <Modal
          {...signupData}
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          content={<div>TODO form</div>}
        />
      )}
      <ThemeContext.Provider value={theme}>
        <main className={theme.backgroundPage}>
          <section>
            <HeroSimpleCentered
              {...HeroSimpleCenteredData}
              //   NavBarData={NavBarData}
            />
          </section>
          <section>
            <FeatureWithProduct
              {...FeatureFirstData}
              productShot={ScreenFindSVG}
            />
            <section>
              <LargeScreenshotFeatures
                {...diagramData}
                screenshot={DiagSVG2}
                isWithGradient={false}
              />
            </section>
          </section>
          <footer className="h-32 w-full"></footer>
        </main>
      </ThemeContext.Provider>
    </>
  )
}
