import { useState } from 'react'
import { ThemeContext } from '@/components/ThemeContext'

import * as themes from '@/utils/theme'

import HeroSimpleCentered from '@/components/HeroSimpleCentered'

const HeroSimpleCenteredData = {
  companyName: 'fortoo',
  headline: 'fortoo',
}

export const NavBarData = {
  navigation: [
    { name: 'Product', href: '#' },
    { name: 'Features', href: '#' },
    { name: 'Marketplace', href: '#' },
    { name: 'Company', href: '#' },
  ],
  companyName: 'fortoo',
  logo: '/logos/Tailwind_CSS_Logo.svg',
}

export default function Sample() {
  const [isOpen, setIsOpen] = useState(true)
  const theme = themes.getLuiTheme()

  return (
    <ThemeContext.Provider value={theme}>
      <main className={theme.backgroundPage}>
        <section>
          <HeroSimpleCentered
            {...HeroSimpleCenteredData}
            NavBarData={NavBarData}
          />
        </section>
      </main>
    </ThemeContext.Provider>
  )
}
