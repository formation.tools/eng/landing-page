// Source https://tailwindui.com/components/application-ui/application-shells/multi-column#component-aa0e4b496e24d741e984c820f5e938ba
import {
  useContext,
  PropsWithChildren,
  ReactNode,
  Fragment,
  useState,
  useRef,
  useEffect,
} from 'react'

import type { ChartData, ChartArea } from 'chart.js'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js'
import { Chart } from 'react-chartjs-2'
import faker from 'faker'

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
  Filler,
)

const labels = [
  '01/07',
  '02/07',
  '03/07',
  '04/07',
  '05/07',
  '06/07',
  '07/07',
  '08/07',
  '09/07',
  '10/07',
  '11/07',
  '12/07',
  '13/07',
  '14/07',
  '15/07',
  '16/07',
  '17/07',
  '18/07',
  '19/07',
  '20/07',
  '21/07',
  '22/07',
  '23/07',
  '24/07',
  '25/07',
  '26/07',
  '27/07',
  '28/07',
  '29/07',
  '30/07',
]

const colors = ['deepskyblue', 'gold', 'hotpink', 'lightpink']

// https://www.chartjs.org/docs/latest/charts/line.html
export const data: ChartData<'line'> = {
  labels,
  datasets: [
    {
      label: 'Requests',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
      tension: 0.5,
      pointStyle: false,
    },
    {
      label: 'Approvals',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 200 })),
      tension: 0.5,
      pointStyle: false,
    },
  ],
}

function createGradient(ctx: CanvasRenderingContext2D, area: ChartArea) {
  const colorStart = faker.random.arrayElement(colors)
  const colorMid = faker.random.arrayElement(
    colors.filter((color) => color !== colorStart),
  )
  const colorEnd = faker.random.arrayElement(
    colors.filter((color) => color !== colorStart && color !== colorMid),
  )

  const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top)

  gradient.addColorStop(0, colorStart)
  gradient.addColorStop(0.5, colorMid)
  gradient.addColorStop(1, colorEnd)

  return gradient
}

// end

import { ThemeContext } from '@/components/ThemeContext'

import { Logo } from '@/components/Logo'

import AppLayout, { DualPane, SinglePane } from '@/components/app/AppLayout'
import { Sidebar } from '@/components/app/Sidebar'

import MetricsMock from '@/components/app/Metrics'

import {
  Bars3Icon,
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
  XMarkIcon,
} from '@heroicons/react/24/outline'

const navigation = [
  { name: 'Dashboard', href: '#', icon: HomeIcon, current: true },
  { name: 'Team', href: '#', icon: UsersIcon, current: false },
  { name: 'Projects', href: '#', icon: FolderIcon, current: false },
  { name: 'Calendar', href: '#', icon: CalendarIcon, current: false },
  { name: 'Documents', href: '#', icon: DocumentDuplicateIcon, current: false },
  { name: 'Reports', href: '#', icon: ChartPieIcon, current: false },
]
const teams = [
  { id: 1, name: 'Heroicons', href: '#', initial: 'H', current: false },
  { id: 2, name: 'Tailwind Labs', href: '#', initial: 'T', current: false },
  { id: 3, name: 'Workcation', href: '#', initial: 'W', current: false },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const chartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
    legend: { display: false },
  },
  scales: {
    x: {
      grid: { display: false },
      ticks: { display: false, beginAtZero: true },
    },
    y: {
      grid: { display: false },
      ticks: { display: false, beginAtZero: true },
    },
  },
}

export default function Page() {
  // TODO: @stefano Parameterize colors
  const theme = useContext(ThemeContext)

  const [sidebarOpen, setSidebarOpen] = useState(false)

  const stats = [
    {
      name: 'Total queries',
      current: '1027',
      previous: '842',
      change: '21.97%',
      changeType: 'increase',
    },
    {
      name: '1st-round acceptance rate',
      current: '57.27%',
      previous: '56.14%',
      change: '2.02%',
      changeType: 'increase',
    },
    {
      name: '2nd-round acceptance rate',
      current: '78.73%',
      previous: '60.23%',
      change: '30.72%',
      changeType: 'increase',
    },
    {
      name: 'Clarification queries',
      current: '24.57%',
      previous: '28.62%',
      change: '4.05%',
      changeType: 'decrease',
    },
  ]

  const periods = [
    {
      id: 'filter-window-1y',
      name: '12 months',
      isActive: false,
      tabIndex: -1,
      ariaControlsId: 'main-view',
    },
    {
      id: 'filter-window-3mo',
      name: '3 months',
      isActive: false,
      tabIndex: -1,
      ariaControlsId: 'main-view',
    },
    {
      id: 'filter-window-30d',
      name: '30 days',
      isActive: true,
      tabIndex: 0,
      ariaControlsId: 'main-view',
    },
    {
      id: 'filter-window-7d',
      name: '7 days',
      isActive: false,
      tabIndex: -1,
      ariaControlsId: 'main-view',
    },
  ]

  const chartRef = useRef<ChartJS>(null)
  const [chartData, setChartData] = useState<ChartData<'line'>>({
    datasets: [],
  })

  useEffect(() => {
    const chart = chartRef.current

    if (!chart) {
      return
    }

    const chartData = {
      ...data,
      datasets: data.datasets.map((dataset) => ({
        ...dataset,
      })),
    }

    setChartData(chartData)
  }, [])

  return (
    <AppLayout sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen}>
      <SinglePane showSidebar={() => setSidebarOpen(true)}>
        <MetricsMock stats={stats} periods={periods} />
        {/* TODO: @stefano width-fit the graph */}
        <div
          className={'mt-5 rounded-lg ring-1 ring-gray-200 lg:h-96'}
          style={{ height: '100%', width: '100%' }}
        >
          <Chart
            ref={chartRef}
            type="line"
            data={chartData}
            options={chartOptions}
          />
        </div>
      </SinglePane>
    </AppLayout>
  )
}
