// Source https://tailwindui.com/components/application-ui/application-shells/multi-column#component-aa0e4b496e24d741e984c820f5e938ba
import { useContext, Fragment, useState } from 'react'

import { ThemeContext } from '@/components/ThemeContext'

import { Logo } from '@/components/Logo'

import AppLayout, { DualPane } from '@/components/app/AppLayout'

import { Sidebar } from '@/components/app/Sidebar'

import {
  Bars3Icon,
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
  XMarkIcon,
} from '@heroicons/react/24/outline'

const navigation = [
  { name: 'Dashboard', href: '#', icon: HomeIcon, current: true },
  { name: 'Team', href: '#', icon: UsersIcon, current: false },
  { name: 'Projects', href: '#', icon: FolderIcon, current: false },
  { name: 'Calendar', href: '#', icon: CalendarIcon, current: false },
  { name: 'Documents', href: '#', icon: DocumentDuplicateIcon, current: false },
  { name: 'Reports', href: '#', icon: ChartPieIcon, current: false },
]
const teams = [
  { id: 1, name: 'Heroicons', href: '#', initial: 'H', current: false },
  { id: 2, name: 'Tailwind Labs', href: '#', initial: 'T', current: false },
  { id: 3, name: 'Workcation', href: '#', initial: 'W', current: false },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function Page() {
  // TODO: @stefano Parameterize colors
  const theme = useContext(ThemeContext)
  const [sidebarOpen, setSidebarOpen] = useState(false)

  return (
    <AppLayout sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen}>
      <DualPane showSidebar={() => setSidebarOpen(true)} aside={'aside'}>
        main
      </DualPane>
    </AppLayout>
  )
}
