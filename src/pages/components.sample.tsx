import { createContext } from 'react'

import NavBar from '@/components/NavBar'
import HeroSimpleCentered from '@/components/HeroSimpleCentered'
import FormSingleInputHorizontal from '@/components/InputForm'
import { PrimaryFeatures } from '@/components/PrimaryFeatures'
import FeatureWithProduct from '@/components/FeatureWithProduct'
import LogoCloudLightTextOnTopRow from '@/components/LogoCloudLightTextOnTopRow'

import { FullScreenshotHero, Hero } from '@/components/Hero'
import { Pricing, PricingNew } from '@/components/Pricing'
import { QuestionsNew } from '@/components/Questions'

import { Button } from '@/components/Button'
import { CallToAction } from '@/components/CallToAction'
import { Container } from '@/components/Container'
import { Footer } from '@/components/Footer'

import * as themes from '@/utils/theme'
import { ThemeContext } from '@/components/ThemeContext'
const theme = themes.getBasicTheme()
/* Newest components */

// FormSingleInputHorizontal && FormDualInputHorizontal

const form = {
  initialValues: {
    email: '',
  },
  validate: (values) => {
    // Implement your form validation logic here
    const errors = {}
    // ...
    return errors
  },
  onSuccess: () => {
    // Handle successful form submission
    // ...
  },
  onSubmit: (values) => {
    // Handle form submission
    // ...
  },
  elements: [
    {
      name: 'email',
      inputType: 'email',
      label: 'Email',
      isRequired: true,
      placeholder: 'Enter your email',
      autocomplete: 'email',
    },
    // Add more form input elements as needed
  ],
}

/* Newer components */

// FullScreenshotHero
const fullScreenshotHeroProps = {
  title: 'Hero Title',
  text: 'Some text describing the hero section.',
  labelCallToAction: 'Call to Action',
  labelMore: 'Learn More',
  screenshot: require('/public/assets/sample/tailwind/project-app-screenshot.png'),
}

// PricingNew

// Props for Pricing component

// Props for PricingNew component
const pricingNewProps = {
  isDark: true,
  frequencies: [
    { value: 'monthly', label: 'Monthly', priceSuffix: '/month' },
    { value: 'annually', label: 'Annually', priceSuffix: '/year' },
  ],
  tiers: [
    {
      name: 'cafe-mode',
      id: 'cafe-mode',
      href: '/waitlist',
      price: { monthly: '$0', annualy: '$0' },
      description:
        'Good for early founding teams still in the idea stage and with the guts to #buildinpublic.',
      features: [
        '1✕ forge org (public repos only)',
        '1✕ IM scope (Slack org)',
        '1✕ file store (Dropbox or GDrive)',
        '5✕ team seats',
        '2✕ (temporary) guest seats',
        'Community Support',
      ],
      mostPopular: false,
    },
    {
      name: 'garage-mode',
      id: 'garage-mode',
      href: '/waitlist',
      price: { monthly: '$5/seat', annualy: '$50/seat' },
      description:
        'Good for teams that have grown beyond the founding team and have have some secret sauce to manage.',
      features: [
        '1✕ forge scope (with private repos)',
        '1✕ IM scope (Slack org)',
        '1✕ file store (Dropbox or GDrive)',
        'Up to 40✕ seats',
        '5✕ (temporary) guest seats',
        'Within 24hrs Support',
        'Personal Inbox',
        'Cataloging Assistance (GPT-3 powered)',
        'Content Personalization (GPT-3 powered)',
        'SSO',
      ],
      mostPopular: true,
    },
    {
      name: 'office-mode',
      id: 'office-mode',
      href: '/waitlist',
      price: { monthly: '$10/seat', annualy: '$100/seat' },
      description:
        'When things move fast and you need to check more boxes. Simplify work across more silos and larger teams',
      features: [
        '2✕ forge scope (GitLab or GitHub)',
        '5✕ IM scope (Slack org)',
        '5✕ file stores (Dropbox or GDrive)',
        'Up to 200✕ seats',
        '15✕ (temporary) guest seats',
        'Within 24hrs Support',
        'Personal Inbox',
        'Cataloging Assistence (GPT-3 powered)',
        'Content Personalization (GPT-3 powered)',
        'SSO',
        'Audit Logs',
      ],
      mostPopular: false,
    },
  ],
}

const isDark = true // Set to `true` for dark theme, or `false` for light theme

const ProductShot = () => (
  <img
    src="/assets/sample/tailwind/project-app-screenshot.png"
    alt="Product Shot"
  />
)

const Example = ({ children }) => (
  <section className="relative m-8 min-h-[200px] border-2">{children}</section>
)

export default function ComponentSamplePage() {
  return (
    <ThemeContext.Provider value={theme}>
      <main className={theme.backgroundPage}>
        <div>
          <section id="Newest Components">
            <h1 className="border-b-2 border-black bg-white py-2 text-center text-2xl ">
              Newest Components
            </h1>

            <h2 className="bg-white py-2 text-center text-xl ">
              NavBar & HeroSimpleCentered
            </h2>

            <Example>
              <NavBar
                navigation={[
                  { name: 'Home', href: '#' },
                  { name: 'About', href: '#' },
                  { name: 'Services', href: '#' },
                ]}
                companyName="Your Company Name"
                logo="/assets/sample/SVG_Logo.svg"
              />
            </Example>

            <Example>
              <HeroSimpleCentered
                headline="Yellowest Lemons"
                companyName="Lemons Inc."
              />
            </Example>

            <h2 className="bg-white py-2 text-center text-xl ">
              PrimaryFeatures
            </h2>
            <h2 className="bg-white py-2 text-center text-xl ">
              FormSingleInputHorizontal
            </h2>

            <Example>
              <FormSingleInputHorizontal
                title="Join our newsletter"
                subtitle="Stay updated with the latest news"
                thanksLabel="Thank you for subscribing!"
                tryAgainLabel="Try Again"
                form={{
                  initialValues: {
                    email: '',
                  },
                  validate: (values) => ({}),
                  onSuccess: () => {},
                  onSubmit: (values) => {},
                  elements: [
                    {
                      name: 'email',
                      inputType: 'email',
                      label: 'Email',
                      isRequired: true,
                      placeholder: 'Enter your email',
                      autocomplete: 'email',
                    },
                    {
                      label: 'Give me early access!',
                      name: 'submit',
                      inputType: 'submit',
                    },
                  ],
                }}
              />
            </Example>

            <h2 className="bg-white py-2 text-center text-xl ">
              previously FormDualInputHorizontal
            </h2>

            <Example>
              <FormSingleInputHorizontal
                title="Join our newsletter"
                subtitle="Stay updated with the latest news"
                thanksLabel="Thank you for subscribing!"
                tryAgainLabel="Try Again"
                form={{
                  initialValues: {
                    email: '',
                    name: '',
                  },
                  validate: (values) => ({}),
                  onSuccess: () => {},
                  onSubmit: (values) => {},
                  elements: [
                    {
                      name: 'email',
                      inputType: 'email',
                      label: 'Email',
                      isRequired: true,
                      placeholder: 'Enter your email',
                      autocomplete: 'email',
                    },
                    {
                      label: 'Name',
                      name: 'name',
                      placeholder: 'Beeblebrox',
                      inputType: 'string',
                      isRequired: true,
                    },
                    {
                      label: 'Sign up',
                      name: 'submit',
                      inputType: 'submit',
                    },
                  ],
                }}
              />
            </Example>

            <h2 className="bg-white py-2 text-center text-xl ">
              LogoCloudLightTextOnTopRow
            </h2>

            <Example>
              <LogoCloudLightTextOnTopRow
                headline="Logo Cloud Headline"
                text="Some text describing the logos"
                CTA={{
                  buttonOne: 'Create account',
                  buttonTwo: 'Contact us',
                }}
                logos={[
                  <img
                    key="reform"
                    src="/assets/sample/tailwind/reform-logo-gray-900.svg"
                    alt="some logo"
                  />,
                  <img
                    key="savvycal"
                    src="/assets/sample/tailwind/savvycal-logo-gray-900.svg"
                    alt="some logo"
                  />,
                  <img
                    key="statamic"
                    src="/assets/sample/tailwind/statamic-logo-gray-900.svg"
                    alt="some logo"
                  />,
                  <img
                    key="transistor"
                    src="/assets/sample/tailwind/transistor-logo-gray-900.svg"
                    alt="some logo"
                  />,
                  <img
                    key="tuple"
                    src="/assets/sample/tailwind/tuple-logo-gray-900.svg"
                    alt="some logo"
                  />,
                ]}
              />
            </Example>
          </section>

          <section id="Newer Components">
            <h1 className="border-b-2 border-black bg-white py-2 text-center text-2xl ">
              Newer Components
            </h1>
            <h2 className="bg-white py-2 text-center text-xl ">Hero</h2>

            <Example>
              <Hero />
            </Example>

            <h2 className="bg-white py-2 text-center text-xl ">
              FullScreenshotHero
            </h2>

            <Example>
              <FullScreenshotHero {...fullScreenshotHeroProps} />
            </Example>

            <h2 className="bg-white py-2 text-center text-xl ">Pricing</h2>
            <Example>
              <Pricing />
            </Example>
            <h2 className="bg-white py-2 text-center text-xl ">PricingNew</h2>
            <Example>
              <PricingNew {...pricingNewProps} />
            </Example>
            <h2 className="bg-white py-2 text-center text-xl ">QuestionsNew</h2>
            <Example>
              <QuestionsNew
                questions={[
                  {
                    question: 'What is the question?',
                    answer: 'This is the answer to the question.',
                  },
                  {
                    question: 'Another question',
                    answer: 'This is the answer to another question.',
                  },
                ]}
                isDark={isDark}
              />
            </Example>
          </section>
          <section id="Old Components">
            <h1 className="border-b-2 border-black bg-white py-2 text-center text-2xl ">
              Old Components
            </h1>
            <h2 className="bg-white py-2 text-center text-xl ">Button</h2>
            <Example>
              <Button variant="solid" color="slate" className="" href="#">
                Click me
              </Button>
            </Example>
            <h2 className="bg-white py-2 text-center text-xl ">CallToAction</h2>
            <Example>
              <CallToAction />
            </Example>
            <h2 className="bg-white py-2 text-center text-xl ">Container</h2>
            <Example>
              <Container>
                <p className="text-white">Hello Container!</p>
              </Container>
            </Example>
            <h2 className="bg-white py-2 text-center text-xl ">Footer</h2>
            <Example>
              <Footer />
            </Example>
          </section>
          <section>
            <Example>
              <FeatureWithProduct
                features={[
                  {
                    name: 'Feature 1',
                    description: 'Description for Feature 1',
                    isWithIcon: true,
                    icon: () => <svg>Icon 1</svg>,
                  },
                  {
                    name: 'Feature 2',
                    description: 'Description for Feature 2',
                    isWithIcon: false,
                  },
                  {
                    name: 'Feature 3',
                    description: 'Description for Feature 3',
                    isWithIcon: true,
                    icon: () => <svg>Icon 3</svg>,
                  },
                ]}
                headline={'Example Headline'}
                tagline={'Example Tagline'}
                text={'Example Text'}
                productShot={ProductShot}
              />
            </Example>
          </section>
        </div>
      </main>
    </ThemeContext.Provider>
  )
}
