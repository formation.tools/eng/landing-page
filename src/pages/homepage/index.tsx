// pages/home/index.tsx

import { fetchPageData } from '../../utils/GROQueries'
import LandingPage, { LandingPageData } from '@/components/LandingPage'
import { GetStaticPropsResult } from 'next'

interface HomePageData extends LandingPageData {}
interface HomePageProps {
  homePageData: HomePageData
}

export async function getStaticProps(): Promise<
  GetStaticPropsResult<HomePageProps>
> {
  const homePageData = await fetchPageData()

  return {
    props: {
      homePageData,
    },
  }
}

export default function HomePage({ homePageData }: HomePageProps) {
  return <LandingPage landingPageData={homePageData} />
}
