import { useContext, useEffect, useState } from 'react'
import { ThemeContext } from '@/components/ThemeContext'

import { Auth } from '@supabase/auth-ui-react'
import { ThemeSupa } from '@supabase/auth-ui-shared'
import { useSession, useSupabaseClient } from '@supabase/auth-helpers-react'

import { Logo } from '@/components/Logo'
import AccountSettings from '@/components/app/AccountSettingsLayout'

const inviteLines = [
  'Sign in to your account',
  'Let us get some work done!',
  'Imagine what we can get done with AI today.',
  'Time to be productive. AI-powered, superproductive!',
  "Isn't it cool that we no longer have to imagine being superhuman?!?",
  'Simon says, augment your AI today!',
  "In a few years, using AI will be as basic as swiping right. Let's go!",
]

const getRandomInvitation = () => {
  const idx = Math.floor(Math.random() * inviteLines.length)
  return inviteLines[idx]
}

// Source https://tailwindui.com/components/application-ui/forms/sign-in-forms#component-bc08eb211afa45fad7c9f89c1891f284
function LoginFormLayout({ children }) {
  const [invitation, setInvitation] = useState('')
  useEffect(() => setInvitation(getRandomInvitation()), [])

  const theme = useContext(ThemeContext)

  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <Logo className={`mx-auto h-10 w-10 ${theme.icon}`} />
          <h2
            className={`mt-6 text-center text-2xl font-bold leading-9 tracking-tight ${theme.textPrimary}`}
          >
            {invitation}
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-[480px]">
          <div className="bg-white px-6 py-12 shadow sm:rounded-lg sm:px-12">
            {children}
          </div>
        </div>
      </div>
    </>
  )
}

const getURL = () => {
  let url =
    process?.env?.NEXT_PUBLIC_SITE_URL ?? // Set this to your site URL in production env.
    process?.env?.NEXT_PUBLIC_VERCEL_URL ?? // Automatically set by Vercel.
    'http://localhost:3000'
  // Make sure to include `https://` when not localhost.
  url = url.includes('http') ? url : `https://${url}`
  // Make sure to including trailing `/`.
  url = url.charAt(url.length - 1) === '/' ? url : `${url}/`
  return url
}

export default function AccountPage() {
  const session = useSession()
  const supabase = useSupabaseClient()

  const theme = useContext(ThemeContext)
  useEffect(() => {
    supabase.auth.onAuthStateChange(async (event, session) => {
      //   console.log('event', event)
      //   console.log('session', session)
      if (event == 'PASSWORD_RECOVERY') {
        const newPassword = prompt(
          'What would you like your new password to be?',
        )
        if (newPassword === null) {
          alert('No password registered!')
          return
        }
        const { data, error } = await supabase.auth.updateUser({
          password: newPassword,
        })

        if (data) alert('Password updated successfully!')
        if (error) alert('There was an error updating your password.')
      }
    })
  }, [supabase.auth])

  if (session) {
    console.info(session, 'session')
    return (
      <AccountSettings
        name={session?.user?.user_metadata?.full_name}
        avatar={session?.user?.user_metadata?.avatar_url}
      />
    )
  }

  return (
    <main className={`${theme.backgroundPage} h-screen w-full`}>
      <LoginFormLayout>
        <Auth
          supabaseClient={supabase}
          appearance={{ theme: ThemeSupa }}
          providers={['google']}
          onlyThirdPartyProviders={true}
          redirectTo={getURL()}
          theme="light"
        />
      </LoginFormLayout>
    </main>
  )
}
