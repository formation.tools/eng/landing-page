// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

import { z } from 'zod'

import { insertRecord } from '@/utils/google'

// TODO: Add to docs
const spreadsheetId = process.env.GOOGLE_SHEETS_SPREADSHEET_ID

const url = process.env.VERCEL_URL

const gitCommitSha = process.env.VERCEL_GIT_COMMIT_SHA
const gitRepoProvider = process.env.VERCEL_GIT_PROVIDER
const gitRepoSlug = process.env.VERCEL_GIT_REPO_SLUG

export const waitlistRegistrationSchema = z.object({
  name: z.string().min(1),
  email: z.string().email(),
  role: z.string().optional(),
  origin: z.string().url().optional(),
})

export const platformSuggestionSchema = z.object({
  email: z.string().email(),
  platform: z.string().min(3),
  origin: z.string().url().optional(),
})

export const integrationSuggestionSchema = z.object({
  email: z.string().email(),
  integration: z.string().min(3),
  origin: z.string().url().optional(),
})

function random(xs: Array<string>): string {
  if (xs.length == 0) {
    return 'invalid'
  }
  const idx = Math.floor(Math.random() * xs.length)
  // NOTE: this happens on the backend
  console.log('getting', idx, 'from', xs.length)
  return xs[idx]
}

type Features = {
  kind: any
  sheet: string | undefined
  cells: any[]
}

type ParseOutput = {
  data: any
  sheet: string
  cellBuilder: (x: any) => any[]
}

function extract(data: any): ParseOutput | undefined {
  switch (data?.kind) {
    case 'registration':
      return {
        data: waitlistRegistrationSchema.parse(data),
        sheet: 'Registrations',
        cellBuilder: (result) => {
          const { name, email, role, origin } = result
          return [name, email, role, origin]
        },
      }

    case 'platform-suggestion':
      return {
        data: platformSuggestionSchema.parse(data),
        sheet: 'Platform Requests',
        cellBuilder: (result) => {
          const { email, platform, origin } = result
          return [email, platform, origin]
        },
      }
    case 'integration-suggestion':
      return {
        data: integrationSuggestionSchema.parse(data),
        sheet: 'Integration Requests',
        cellBuilder: (result) => {
          const { email, integration, origin } = result
          return [email, integration, origin]
        },
      }

    default:
      break
  }

  return undefined
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const coolPerson = random([
    // Imagine this being a call from a database 🙈
    'Linus Torvalds',
    'John Carmack',
    'Bryan Cantrill',
    'Claire Wolf',
    'Andrew Kelley',
    'Dave Wilson (SystemCrafters)',
    'George Hotz',
    'Graham Hutton',
    'Jessie Frazelle',
    'John Wiegley',
    'Lex Fridman',
    'Mara Bos',
    'Oleh Krehel',
    'Paul Graham',
    'Sacha Chua',
    'Steve Purcell',
    'ThePrimeagen',
    'Theo Browne (t3.gg)',
    'Yannic Kilcher',
    'tecosaur',
  ])

  try {
    const json = JSON.parse(req.body)
    const result = extract(json)

    if (result === undefined) {
      console.log(result)
      res.status(400).json({
        error: [
          'Information submitted does not appear valid.',
          'Please update your information and try again',
          `or e-mail us at ${process.env.EMAIL_WAITLIST}.`,
        ].join('\n'),
        coolPerson,
      })
      return
    }

    const meta = [
      new Date().toISOString(),
      gitRepoProvider,
      gitRepoSlug,
      gitCommitSha,
    ]

    const cells = result.cellBuilder(result.data)

    if (spreadsheetId === undefined) {
      throw new Error('spreadsheetId is undefined')
    }

    await insertRecord(spreadsheetId, result.sheet, [...cells, ...meta])

    res.status(200).json({
      message: 'Registered!',
      coolPerson,
    })
  } catch (error) {
    console.error(error)
    res.status(400).json({
      error: 'Submission failed due to an issue on our end.',
      coolPerson,
    })
  }
}
