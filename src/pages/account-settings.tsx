import AccountSettings from '@/components/app/AccountSettingsLayout'

import * as themes from '@/utils/theme'
import { ThemeContext } from '@/components/ThemeContext'
const theme = themes.getBasicTheme()

const Example = ({ children }) => (
  <section className="relative m-8 min-h-[200px] border-2">{children}</section>
)

export default function AccountSettingsSamplePage() {
  return (
    <ThemeContext.Provider value={theme}>
      <AccountSettings />
    </ThemeContext.Provider>
  )
}
