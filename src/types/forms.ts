import { FormikValues } from 'formik'

export type WaitlistData = FormikValues & {
  name: string
  email: string
  role?: string
}

export type PlatformSuggestionData = FormikValues & {
  platform: string
  email: string
}

export type IntegrationSuggestionData = FormikValues & {
  integration: string
  email: string
}
