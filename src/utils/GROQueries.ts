// TODO: make scalabale - the query should be generated based on the schema of the Sanity types
import { client } from '../../sanity/lib/client'

export const fullScreenshotHeroQuery = `
  _type,
  _id,
  title,
  text,
  labelCallToAction,
  labelMore,
  "image": image.asset->url
`

export const heroSimpleCenteredQuery = `
  _type,
  _id,
  headline,
  "navBarData": navBarData->{
    _type,
    _id, 
    companyName,
    "logo": logo.asset->url,
    navigation[]{
      linkName,
      linkUrl
    }
  }
`

export const pricingNewQuery = `
  _type,
  _id,
  frequencies[] {
    value,
    label,
    priceSuffix
  },
  tiers[] {
    name,
    id,
    href,
    price {
      monthly,
      annualy
    },
    description,
    features,
    mostPopular,
    mostPopularLabel
  }
`

export const logoCloudLightTextOnTopRowQuery = `
  _type,
  _id,
  headline,
  text,
  CTA {
    buttonOne,
    buttonTwo
  },
  logos[] {
    asset->{
      url
    }
  }
`

export const formSingleInputHorizontalQuery = `
  _type,
  _id,
  title,
  subtitle,
  thanksLabel,
  tryAgainLabel,
  formData
`

export const featureWithProductQuery = `
_type,
_id,
tagline,
headline,
text,
features[]{
  name,
  description,
  "icon": icon.asset->url
},
"productShot": productShot.asset->url
`

export const questionsNewQuery = `
  _type,
  _id,
  isDark,
  questions[] {
    question,
    answer
  }
`

export const termsAndConditionsQuery = `
_type,
_id,
title,
portableTextContent
`

export const privacyPolicyQuery = `
_type,
_id,
title,
portableTextContent
`
export async function fetchPageData(id?: string) {
  const query = id
    ? `*[_type == "landingPage" && slug.current == $id]{
      title,
	  themeName,
      elements[]-> {
        _type == "fullScreenshotHero" => {
          ${fullScreenshotHeroQuery}
        },
        _type == "pricingNew" => {
          ${pricingNewQuery}
        },
        _type == "logoCloudLightTextOnTopRow" => {
          ${logoCloudLightTextOnTopRowQuery}
        },
        _type == "formSingleInputHorizontal" => {
          ${formSingleInputHorizontalQuery}
        },
        _type == "questionsNew" => {
          ${questionsNewQuery}
        },
        _type == "termsAndConditions" => {
          ${termsAndConditionsQuery}
        },
        _type == "privacyPolicy" => {
          ${privacyPolicyQuery}
        },
        _type == "heroSimpleCentered" => {
          ${heroSimpleCenteredQuery}
        },
        _type == "featureWithProduct" => {
          ${featureWithProductQuery}
        },
      }
    }`
    : `*[_type == "homePage"][0]{
		"landingPageData": landingPage->{
		  title,
		  themeName,
		  elements[]-> {
			_type == "fullScreenshotHero" => {
			  ${fullScreenshotHeroQuery}
			},
			_type == "pricingNew" => {
			  ${pricingNewQuery}
			},
			_type == "logoCloudLightTextOnTopRow" => {
			  ${logoCloudLightTextOnTopRowQuery}
			},
			_type == "formSingleInputHorizontal" => {
			  ${formSingleInputHorizontalQuery}
			},
			_type == "questionsNew" => {
			  ${questionsNewQuery}
			},
			_type == "termsAndConditions" => {
			  ${termsAndConditionsQuery}
			},
			_type == "privacyPolicy" => {
			  ${privacyPolicyQuery}
			},
			_type == "heroSimpleCentered" => {
			  ${heroSimpleCenteredQuery}
			},
			_type == "featureWithProduct" => {
			  ${featureWithProductQuery}
			},
		  }
		}
	  }`
  const data = id
    ? await client.fetch(query, { id })
    : await client.fetch(query)

  if (!data || (id && !data[0]) || (!id && !data.landingPageData)) {
    throw new Error('Page not found')
  }

  return id ? data[0] : data.landingPageData
}
