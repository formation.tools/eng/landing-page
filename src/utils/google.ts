import { google } from 'googleapis'

const sheets = google.sheets('v4')

export async function insertRecord(
  spreadsheetId: string,
  range: string,
  fields: (string | number)[],
) {
  const scopes = ['https://www.googleapis.com/auth/spreadsheets']
  const jwt = new google.auth.JWT(
    process.env.GOOGLE_SHEETS_CLIENT_EMAIL,
    undefined,
    (process.env.GOOGLE_SHEETS_PRIVATE_KEY || '').replace(/\\n/g, '\n'),
    scopes,
  )

  const sheets = google.sheets({ version: 'v4', auth: jwt })

  const response = await sheets.spreadsheets.values.append({
    spreadsheetId,
    range,
    valueInputOption: 'USER_ENTERED',
    requestBody: {
      values: [fields],
    },
  })

  console.log('Did gsheet append', fields)

  return response.data
}
