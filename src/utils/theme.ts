export type Theme = {
  backgroundPage: string
  accent: string
  label: string
  textPrimary: string
  textSecondary: string
  textAccent: string
  // TODO: Remove and define bg, text and hover in button and buttonActive
  textButton: string
  textActive: string
  sidebar: string
  sidebarIcon: string
  sidebarLogo: string
  sidebarGroup: string
  divide: string
  button: string
  buttonConfirm: string
  buttonPrimary: string
  buttonSecondary: string
  buttonDestructive: string
  buttonDisabled: string
  buttonOutline: string
  buttonToggleRadioGroup: string
  buttonToggleActive: string
  buttonToggleInactive: string
  pricingButtonPopular: string
  pricingButtonRegular: string
  pricingBlockPopular: string
  pricingBlockTitlePopular: string
  pricingBlockLabelStamp: string
  pricingBlockRegular: string
  pricingBlockTextPrimary: string
  pricingBlockTitleRegular: string
  icon: string
  ringScreenshot: string
  ringMobileMenu: string
  background: string
  backgroundAccent: string
  backgroundButton: string
  backgroundDialog: string
  backgroundModal: string
  gradientEdge: string
  divideFaqs: string
  opacity: string
  mostPopular: string
  checkIcon: string
  MobileMenuText: string
  input: string
  inputFocus: string
  inputAccent: string
}

export function getLuiTheme(): Theme {
  return {
    backgroundPage:
      'from-15% to-85% flex  h-fit flex-col bg-gradient-to-tr from-[#753A88] to-[#CC2B5E]',
    accent: '',
    background: 'transparent',
    backgroundAccent: 'transparent',
    backgroundModal: 'bg-black/20',
    textPrimary: 'text-white',
    textSecondary: 'text-white',
    textButton: 'text-gray-400',
    textAccent: 'text-indigo-400',
    textActive: '',
    icon: 'text-white fill-white',
    ringScreenshot: 'ring-gray-700',
    ringMobileMenu: 'ring-pink-300',
    MobileMenuText: 'text-pink-300 hover:bg-pink-300',
    button: 'bg-[#00FFA3] text-black hover:bg-indigo-200',
    buttonConfirm: '',
    buttonPrimary: '',
    buttonSecondary: '',
    buttonDestructive: '',
    buttonDisabled: '',
    buttonToggleRadioGroup: 'bg-white/5 text-white',
    buttonToggleActive: 'bg-indigo-500',
    buttonToggleInactive: '',
    pricingButtonPopular:
      'bg-indigo-200 text-black shadow-sm hover:bg-indigo-400 focus-visible:outline-indigo-500',
    pricingButtonRegular:
      'bg-white/10 text-white hover:bg-white/20 focus-visible:outline-white',
    pricingBlockPopular: 'bg-white/5 ring-2 ring-indigo-500',
    pricingBlockLabelStamp: 'bg-indigo-500 text-white',
    pricingBlockRegular: 'ring-1 ring-white/10',
    pricingBlockTitlePopular: 'text-white',
    pricingBlockTextPrimary: 'text-white',
    pricingBlockTitleRegular: 'text-white',
    backgroundButton: 'bg-indigo-500 hover:bg-indigo-400',
    buttonOutline: 'focus-visible:outline-indigo-400',
    backgroundDialog: 'bg-gray-400',
    gradientEdge: 'bg-gradient-to-t from-gray-900',
    divide: 'divide-gray-500/2',
    divideFaqs: 'divide-white/10',
    opacity: '.2',
    mostPopular: 'bg-indigo-500 text-white',
    checkIcon: 'text-white',
    input: 'bg-white text-black ring-white/10 focus:ring-indigo-500',
    inputFocus: '',
    inputAccent: '',
    sidebar: '',
    sidebarIcon: '',
    sidebarLogo: '',
    sidebarGroup: '',
    label: '',
  }
}

export function getBasicTheme(): Theme {
  return {
    backgroundPage: 'white',
    accent: 'bg-white border-gray-100 text-gray-500',
    background: 'white',
    backgroundAccent: 'bg-gray-200',
    backgroundModal: 'bg-gray-800/20',
    textPrimary: 'text-black',
    textSecondary: 'text-gray-800',
    textButton: 'text-white',
    textAccent: 'text-black',
    textActive: 'text-black font-bold',
    icon: 'text-black',
    sidebarLogo: 'fill-gray-200 text-gray-200',
    ringScreenshot: 'ring-black',
    ringMobileMenu: 'ring-black',
    MobileMenuText: 'text-black hover:text-gray-900',
    button:
      'bg-black text-white hover:bg-gray-300 hover:text-black group-hover:text-white',
    buttonSecondary: 'bg-gray-300 text-black',
    // bg-indigo-500 text-white hover:bg-indigo-400 focus-visible:outline-indigo-500
    buttonConfirm: 'bg-green-300',
    buttonPrimary: 'bg-gray-200 text-black',
    buttonDestructive: 'bg-red-300',
    buttonDisabled: '',
    buttonOutline:
      'focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-black',
    buttonToggleRadioGroup: 'bg-gray-100',
    buttonToggleActive: 'bg-black text-white',
    buttonToggleInactive: 'text-gray-600',
    pricingButtonPopular:
      'bg-black text-white shadow-sm hover:bg-gray-900 focus-visible:outline-black',
    pricingButtonRegular:
      'bg-gray-200 text-black hover:bg-black hover:text-white',
    pricingBlockPopular: 'bg-white/5 ring-2 ring-black',
    pricingBlockTitlePopular: 'text-white',
    pricingBlockLabelStamp: 'bg-black text-white',
    pricingBlockRegular: 'hover:ring-2 hover:ring-gray-300',
    pricingBlockTextPrimary: 'text-black',
    pricingBlockTitleRegular: 'text-black',
    backgroundButton: 'bg-gray-400 hover:bg-gray-200',
    backgroundDialog: 'bg-gray-200',
    gradientEdge: 'bg-gradient-to-t from-gray-900',
    divideFaqs: 'divide-white/10',
    opacity: '.2',
    mostPopular: 'bg-gray-200 text-black',
    checkIcon: 'text-black',
    sidebar: 'bg-gray-100 text-gray-500 ring-white/5',
    sidebarGroup: 'group-hover:text-white',
    sidebarIcon: 'border-2 border-gray-500',
    divide: 'divide-gray-200',
    //input: 'bg-white text-black ring-gray-300',
    input: 'bg-white text-black ring-gray-300 focus:ring-black',
    inputFocus: 'ring-red-300',
    inputAccent: 'placeholder:text-gray-500 text-black',
    label: 'text-black',
  }
}
