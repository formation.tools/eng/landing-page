const config = {
  title: 'fortoo -- AI-powered productivity made simple',
  siteUrl: 'https://www.fortoo.io',
  social: {
    twitter: '@fortoolabs',
    //linkedin: 'example',
    //facebook: 'example',
  },
}

export default config
