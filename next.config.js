/** @type {import('next').NextConfig} */

const withOnlyDev = (config) => {
  if (process.env.NODE_ENV === 'development') {
    return [...config, 'tsx', 'ts', 'jsx', 'js']
  }

  return config
}

const nextConfig = {
  reactStrictMode: true,
  experimental: {
    scrollRestoration: true,
  },
  images: {
    domains: ['tailwindui.com', 'cdn.sanity.io'],
  },
  async redirects() {
    return [
      {
        source: '/waitlist',
        permanent: false,
        destination: 'https://h8d701bcme8.typeform.com/to/WTkrP8Cx',
      },
    ]
  },
  webpack(config) {
    // Grab the existing rule that handles SVG imports
    const fileLoaderRule = config.module.rules.find((rule) =>
      rule.test?.test?.('.svg'),
    )

    config.module.rules.push(
      // Reapply the existing rule, but only for svg imports ending in ?url
      {
        ...fileLoaderRule,
        test: /\.svg$/i,
        resourceQuery: /url/, // *.svg?url
      },
      // Convert all other *.svg imports to React components
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        resourceQuery: { not: /url/ }, // exclude if *.svg?url
        use: ['@svgr/webpack'],
      },
    )
    // Modify the file loader rule to ignore *.svg, since we have it handled now.
    fileLoaderRule.exclude = /\.svg$/i

    return config
  },

  pageExtensions: withOnlyDev([
    'page.js',
    'page.jsx',
    'page.ts',
    'page.tsx',
    'md',
    'mdx',
  ]),
}

// log the page extensions

module.exports = nextConfig
