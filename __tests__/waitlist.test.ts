import { submissionSchema } from '@/pages/api/waitlist'

describe('waitlist submission', () => {
  const parse = (data) => submissionSchema.safeParse(data)

  it('must have a name', () => {
    const base = {
      email: 'potg@milkyway.galaxy',
    }

    expect(parse({ ...base }).success).toBe(false)

    expect(parse({ ...base, name: '' }).success).toBe(false)

    expect(parse({ ...base, name: 'Zaphod' }).success).toBe(true)
  })

  it('may have a role', () => {
    const base = {
      name: 'Zaphod',
      email: 'potg@milkyway.galaxy',
    }

    expect(parse({ ...base }).success).toBe(true)

    expect(parse({ ...base, role: '' }).success).toBe(true)

    expect(parse({ ...base, role: 'President of the Galaxy' }).success).toBe(
      true,
    )
  })

  it('must have a valid email address', () => {
    const base = {
      name: 'Zaphod',
    }

    expect(parse({ ...base }).success).toBe(false)

    expect(parse({ ...base, email: '' }).success).toBe(false)

    expect(parse({ ...base, email: 'potg@milkyway' }).success).toBe(false)

    expect(parse({ ...base, email: 'potg@milkyway.galaxy' }).success).toBe(true)
  })

  it('may have an origin', () => {
    const base = {
      name: 'Zaphod',
      email: 'potg@milkyway.galaxy',
    }

    expect(parse({ ...base }).success).toBe(true)

    expect(parse({ ...base, origin: '' }).success).toBe(false)

    expect(parse({ ...base, origin: 'localhost' }).success).toBe(false)

    expect(parse({ ...base, origin: 'localhost:3000' }).success).toBe(true)

    expect(
      parse({ ...base, origin: 'example.com/this/is/serious' }).success,
    ).toBe(false)

    expect(
      parse({ ...base, origin: 'https://example.com/this/is/serious' }).success,
    ).toBe(true)
  })
})
