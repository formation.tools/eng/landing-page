import { type SchemaTypeDefinition } from 'sanity'

import landingPage from './schemas/landingPage'
import homePage from './schemas/homePage'
import fullScreenshotHero from './schemas/fullScreenshotHero'
import heroSimpleCentered from './schemas/heroSimpleCentered'
import navBar from './schemas/navBar'
import pricingNew from './schemas/pricingNew'
import formSingleInputHorizontal from './schemas/formSingleInputHorizontal'
import questionsNew from './schemas/questionsNew'
import logoCloudLightTextOnTopRow from './schemas/logoCloudLightTextOnTopRow'
import termsAndConditions from './schemas/termsAndConditions'
import privacyPolicy from './schemas/privacyPolicy'
import product from './schemas/product'
import article from './schemas/article'
import blockContent from './schemas/blockContent'
import featureWithProduct from './schemas/featureWithProduct'

export const schema: { types: SchemaTypeDefinition[] } = {
  types: [
    blockContent,
    landingPage,
    homePage,
    fullScreenshotHero,
    heroSimpleCentered,
    navBar,
    pricingNew,
    formSingleInputHorizontal,
    questionsNew,
    logoCloudLightTextOnTopRow,
    termsAndConditions,
    privacyPolicy,
    featureWithProduct,
    product,
    article,
  ],
}
