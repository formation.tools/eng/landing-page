import { defineType, defineField } from 'sanity'

const homePage = defineType({
  name: 'homePage',
  title: 'Home Page',
  type: 'document',
  fields: [
    defineField({
      name: 'landingPage',
      title: 'Landing Page',
      type: 'reference',
      to: [{ type: 'landingPage' }],
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'landingPage.internalTitle', // Display the internalTitle of the referenced landingPage as the title in the Studio
    },
  },
})

export default homePage
