import { defineType, defineField } from 'sanity'

const navBar = defineType({
  name: 'navBar',
  title: 'Navigation Bar',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      description: 'It will not be displayed anywhere, only for internal use',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'companyName',
      title: 'Company Name',
      type: 'string',
      description: 'Name of the company',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'logo',
      title: 'Logo',
      type: 'image',
      description: 'Logo of the company',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'navigation',
      title: 'Navigation',
      type: 'array',
      of: [
        {
          type: 'object',
          fields: [
            { name: 'linkName', type: 'string', title: 'Link Name' },
            { name: 'linkUrl', type: 'string', title: 'Link URL' },
          ],
        },
      ],
      description: 'Navigation links',
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle',
    },
  },
})

export default navBar
