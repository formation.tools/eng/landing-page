import { defineType, defineField } from 'sanity'

const privacyPolicy = defineType({
  name: 'privacyPolicy',
  title: 'Privacy Policy',
  type: 'document',
  fields: [
    defineField({
      name: 'internalName',
      title: 'Internal Name',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'The title of the Privacy Policy component',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'portableTextContent',
      title: 'Portable Text Content',
      type: 'array',
      of: [
        {
          type: 'block',
        },
      ],
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalName',
    },
  },
})

export default privacyPolicy
