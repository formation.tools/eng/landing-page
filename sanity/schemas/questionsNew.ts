import { defineType, defineField, defineArrayMember } from 'sanity'

const questionsNew = defineType({
  title: 'Questions New',
  name: 'questionsNew',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      description: 'It will not be displayed anywhere, only for internal use',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'Is Dark',
      name: 'isDark',
      type: 'boolean',
      description: 'Enable dark mode for this component.',
    }),
    defineField({
      title: 'Questions',
      name: 'questions',
      type: 'array',
      of: [
        defineArrayMember({
          type: 'object',
          title: 'Q&A Object',
          name: 'Q&AObject',
          fields: [
            defineField({
              title: 'Question',
              name: 'question',
              type: 'string',
              validation: (Rule) => Rule.required(),
            }),
            defineField({
              title: 'Answer',
              name: 'answer',
              type: 'string',
              validation: (Rule) => Rule.required(),
            }),
          ],
        }),
      ],
      validation: (Rule) => Rule.min(3),
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle',
    },
  },
})

export default questionsNew
