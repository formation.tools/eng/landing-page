import { defineType, defineField } from 'sanity'

const heroSimpleCentered = defineType({
  name: 'heroSimpleCentered',
  title: 'Hero Simple Centered',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'headline',
      title: 'Headline',
      type: 'string',
      description: 'The main headline of the hero',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'navBarData',
      title: 'Navigation Bar Data',
      type: 'reference',
      to: [{ type: 'navBar' }],
      description: 'The reference to the Navigation Bar document',
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle',
    },
  },
})

export default heroSimpleCentered
