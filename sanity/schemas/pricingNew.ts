import { defineType, defineField } from 'sanity'
const pricingNew = defineType({
  name: 'pricingNew',
  title: 'Pricing New',
  type: 'document',
  fields: [
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'frequencies',
      title: 'Frequencies',
      type: 'array',
      of: [
        {
          type: 'object',
          fields: [
            {
              name: 'value',
              title: 'Value',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'label',
              title: 'Label',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'priceSuffix',
              title: 'Price Suffix',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
          ],
          validation: (Rule) => Rule.required(),
        },
      ],
    }),
    defineField({
      name: 'tiers',
      title: 'Tiers',
      type: 'array',
      of: [
        {
          type: 'object',
          fields: [
            {
              name: 'name',
              title: 'Name',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'id',
              title: 'ID',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'href',
              title: 'Href',
              type: 'url',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'price',
              title: 'Price',
              type: 'object',
              fields: [
                {
                  name: 'monthly',
                  title: 'Monthly',
                  type: 'string',
                  validation: (Rule) => Rule.required(),
                },
                {
                  name: 'annualy',
                  title: 'Annually',
                  type: 'string',
                  validation: (Rule) => Rule.required(),
                },
              ],
            },
            {
              name: 'description',
              title: 'Description',
              type: 'string',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'features',
              title: 'Features',
              type: 'array',
              of: [{ type: 'string' }],
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'mostPopular',
              title: 'Most Popular',
              type: 'boolean',
              validation: (Rule) => Rule.required(),
            },
            {
              name: 'mostPopularLabel',
              title: 'Most Popular Label',
              type: 'string',
            },
          ],
          validation: (Rule) => Rule.required(),
        },
      ],
    }),
  ],
})

export default pricingNew
