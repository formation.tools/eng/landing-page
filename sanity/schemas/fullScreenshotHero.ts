import { defineType, defineField } from 'sanity'

const fullScreenshotHero = defineType({
  name: 'fullScreenshotHero',
  title: 'Full Screenshot Hero',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      description: 'It will not be displayed anywhere, only for internal use',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'The title of the hero',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'text',
      title: 'Text',
      type: 'string',
      description: 'The text of the hero',
      validation: (Rule) => Rule.required(),
    }),

    defineField({
      name: 'labelCallToAction',
      title: 'Label Call To Action',
      type: 'string',
      description: 'The label of the call to action button',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'labelMore',
      title: 'Label More',
      type: 'string',
      description: 'The label of the other button',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'image',
      title: 'Image',
      type: 'image',
      description: 'The image of the hero',
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle',
    },
  },
})

export default fullScreenshotHero
