import { defineType, defineField } from 'sanity'

const formSingleInputHorizontal = defineType({
  title: 'Form Single Input Horizontal',
  name: 'formSingleInputHorizontal',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      description: 'It will not be displayed anywhere, only for internal use',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'The title of the form component',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'subtitle',
      title: 'Subtitle',
      description: 'We can add a description here',
      type: 'string',
    }),
    defineField({
      name: 'thanksLabel',
      title: 'Thanks Label',
      type: 'string',
      description: 'The content of the thanks message',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'tryAgainLabel',
      title: 'Try Again Label',
      type: 'string',
      description: 'The content of the try again message',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'formData',
      title: 'Form Data Type',
      type: 'string',
      options: {
        list: [
          {
            title: 'Waitlist Registration Data',
            value: 'WaitlistRegistrationFormData',
          },
          {
            title: 'Integration Suggestion Data',
            value: 'IntegrationSuggestionFormData',
          },
          {
            title: 'Platform Suggestion Data',
            value: 'PlatformSuggestionFormData',
          },
        ],
        layout: 'radio', // or 'dropdown'
      },
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle', // Display internalTitle as the title in the Studio
    },
  },
})

export default formSingleInputHorizontal
