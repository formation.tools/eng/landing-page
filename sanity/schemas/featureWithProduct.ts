import { defineType, defineField } from 'sanity'

const featureWithProduct = defineType({
  name: 'featureWithProduct',
  title: 'Feature With Product',
  type: 'document',
  fields: [
    defineField({
      name: 'internalName',
      title: 'Internal Name',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'tagline',
      title: 'Tagline',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'headline',
      title: 'Headline',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'text',
      title: 'Text',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'features',
      title: 'Features',
      type: 'array',
      of: [
        {
          type: 'object',
          fields: [
            { name: 'name', type: 'string', title: 'Name' },
            { name: 'description', type: 'string', title: 'Description' },
            { name: 'icon', type: 'image', title: 'Icon' },
          ],
        },
      ],
    }),
    defineField({
      name: 'productShot',
      title: 'Product Shot',
      type: 'image',
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalName',
    },
  },
})

export default featureWithProduct
