import { defineType, defineField } from 'sanity'
// import type { Element } from '@/pages/use-case/[id].page'

const landingPage = defineType({
  name: 'landingPage',
  title: 'Landing Page',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'themeName',
      title: 'Theme',
      type: 'string',
      options: {
        list: [
          { title: 'Lui Theme', value: 'lui' },
          { title: 'Basic Theme', value: 'basic' },
        ],
        layout: 'radio',
      },
    }),
    {
      name: 'elements',
      title: 'Elements',
      type: 'array',
      of: [
        {
          type: 'reference',
          to: [
            { type: 'fullScreenshotHero' },
            { type: 'pricingNew' },
            { type: 'formSingleInputHorizontal' },
            { type: 'questionsNew' },
            { type: 'logoCloudLightTextOnTopRow' },
            { type: 'termsAndConditions' },
            { type: 'privacyPolicy' },
            { type: 'heroSimpleCentered' },
            { type: 'featureWithProduct' },
          ],
        },
      ],
      validation: (Rule) =>
        Rule.custom((elements: Element[]) => {
          if (!elements || elements.length === 0) {
            return 'At least one element is required.'
          }

          return true
        }),
    },
    defineField({
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: (_) => `${Date.now()}`.slice(0, 10),
        maxLength: 10,
        slugify: (input) =>
          input.toLowerCase().replace(/\s+/g, '-').slice(0, 10),
        isUnique: async (slug, context) => {
          // https://www.sanity.io/docs/slug-type#d5066a58b95a
          // TODO: @stefano Isolate for reuse
          const { document, getClient } = context
          const client = getClient({ apiVersion: '2023-06-01' })
          if (document) {
            const id = document._id.replace(/^drafts\./, '')
            const params = {
              draft: `drafts.${id}`,
              published: id,
              slug,
            }
            const query = `!defined(*[!(_id in [$draft, $published]) && slug.current == $slug][0]._id)`
            const result = await client.fetch(query, params)
            return result
          }
          return true // if document is undefined, return true as we can't perform the check
        },
      },
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle', // Display internalTitle as the title in the Studio
    },
  },
})

export default landingPage
