import { defineType, defineField } from 'sanity'
import slugify from 'slugify'

export default defineType({
  name: 'Article',
  title: 'Article',
  type: 'document',
  description: 'Content like blogs, case studies and press notes',
  groups: [{ name: 'seo', title: 'SEO' }],
  fields: [
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'seoTitle',
      title: 'SEO Title',
      type: 'string',
      group: 'seo',
    }),
    defineField({
      name: 'publishedAt',
      title: 'Publishing date',
      type: 'datetime',
    }),
    defineField({
      name: 'modifiedAt',
      title: 'Modifying date',
      type: 'datetime',
    }),
    defineField({
      name: 'keywords',
      title: 'Keywords',
      type: 'array',
      of: [{ type: 'string' }],
      validation: (Rule) => Rule.required(),
      group: 'seo',
    }),
    defineField({
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        slugify: (input) => slugify(input),
      },
      group: 'seo',
    }),
    defineField({
      name: 'summary',
      title: 'Summary',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'Poster Image',
      name: 'poster',
      type: 'image',
      description: 'Thumbnail for the article',
      options: { hotspot: true },
      group: ['seo'],
      fields: [
        {
          name: 'caption',
          type: 'string',
          title: 'Caption',
        },
        {
          name: 'attribution',
          type: 'string',
          title: 'Attribution',
        },
      ],
    }),
    defineField({
      name: 'content',
      title: 'Content',
      type: 'array',
      of: [{ type: 'block' }],
    }),
  ],
  initialValue: {
    modifiedAt: new Date().toISOString(),
    publishedAt: new Date().toISOString(),
  },
  preview: {
    select: {
      title: 'title',
      subtitle: 'slug.current',
    },
  },
})
