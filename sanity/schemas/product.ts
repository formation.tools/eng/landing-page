import { defineType, defineField } from 'sanity'
import slugify from 'slugify'

export default defineType({
  name: 'Product',
  title: 'Product',
  type: 'document',
  description: 'Products that fortoo offers',
  fields: [
    defineField({
      name: 'name',
      title: 'Product Name',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'description',
      title: 'Product Description',
      type: 'string',
    }),
    defineField({
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        maxLength: 10,
        slugify: (input) => slugify(input),
        isUnique: async (slug, context) => {
          const { document, getClient } = context
          const client = getClient({ apiVersion: '2023-06-01' })
          if (document) {
            const id = document._id.replace(/^drafts\./, '')
            const params = {
              draft: `drafts.${id}`,
              published: id,
              slug,
            }
            const query = `!defined(*[!(_id in [$draft, $published]) && slug.current == $slug][0]._id)`
            const result = await client.fetch(query, params)
            return result
          }
          return true // if document is undefined, return true as we can't perform the check
        },
      },
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'name', // Display internalTitle as the title in the Studio
    },
  },
})
