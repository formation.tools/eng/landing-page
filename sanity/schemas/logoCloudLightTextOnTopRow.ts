import { defineType, defineField, defineArrayMember } from 'sanity'

const logoCloudLightTextOnTopRow = defineType({
  title: 'Logo Cloud Light Text on Top Row',
  name: 'logoCloudLightTextOnTopRow',
  type: 'document',
  fields: [
    defineField({
      name: 'internalTitle',
      title: 'Internal Title',
      type: 'string',
      description: 'It will not be displayed anywhere, only for internal use',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'Headline',
      name: 'headline',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'Text',
      name: 'text',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'CTA',
      name: 'CTA',
      type: 'object',
      fields: [
        defineField({
          title: 'Button One',
          name: 'buttonOne',
          type: 'string',
        }),
        defineField({
          title: 'Button Two',
          name: 'buttonTwo',
          type: 'string',
        }),
      ],
    }),
    defineField({
      title: 'Logos',
      name: 'logos',
      type: 'array',
      of: [
        defineArrayMember({
          type: 'image',
          title: 'Logo',
          name: 'logo',
        }),
      ],
      validation: (Rule) => Rule.min(3),
    }),
  ],
  preview: {
    select: {
      title: 'internalTitle',
    },
  },
})

export default logoCloudLightTextOnTopRow
