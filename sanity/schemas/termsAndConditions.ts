import { defineType, defineField } from 'sanity'

const termsAndConditions = defineType({
  name: 'termsAndConditions',
  title: 'Terms and Conditions',
  type: 'document',
  fields: [
    defineField({
      name: 'internalName',
      title: 'Internal Name',
      type: 'string',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'The title of the Terms and Conditions component',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      name: 'portableTextContent',
      title: 'Portable Text Content',
      type: 'array',
      of: [
        {
          type: 'block',
        },
      ],
      validation: (Rule) => Rule.required(),
    }),
  ],
  preview: {
    select: {
      title: 'internalName',
    },
  },
})

export default termsAndConditions
