# Logos Integrations

## Notion

https://www.notion.so/Media-Kit-205535b1d9c4440497a3d7a2ac096286

## Confluence

## Microsoft 365

- Microsoft 365 has possibly a new logo
- It's not clare how freely usable it is

## Google Workspace

## Slack

https://salesforce.widencollective.com/c/xt57vnlf

## Microsoft Teams

- check license: https://www.microsoft.com/en-us/legal/intellectualproperty/trademarks

## GitHub

https://github.com/logos
No .svg but .eps or .ai
Ideally, the best format to convert an image to SVG is a vector file format such as .ai or .eps.

## GitLab

https://about.gitlab.com/press/press-kit/

## Read the Docs

https://brand-guidelines.readthedocs.org/branding.html
