/// <reference types="cypress" />

describe('Fortoo Landing Page', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('should display the landing page', () => {
    cy.get('h1').contains('fortoo')
  })

  it('should submit the first form with a valid email', () => {
    const validEmail = 'test@example.com'
    cy.get('form')
      .eq(0)
      .within(() => {
        cy.get('input[name="email"]').should('exist')
        cy.get('input[name="email"]').type(validEmail)
        cy.get('button[type="submit"]').click()
      })
    cy.contains('Thanks! You are waitlisted').should('be.visible')
  })
  //   it('should display an error message with an invalid email', () => {
  //     const invalidEmail = 'invalid_email'

  //     cy.get('form')
  //       .eq(0)
  //       .within(() => {
  //         cy.get('input[name="email"]').type(invalidEmail)
  //         cy.get('button[type="submit"]').click()
  //       })

  //     cy.contains('Please enter a valid email.').should('be.visible')
  //   })

  it('should test the second form with a valid email and platform name "Slack"', () => {
    const validEmail = 'test@example.com'
    const platformName = 'Slack'

    cy.get('form')
      .eq(1)
      .within(() => {
        cy.get('input[name="email"]').type(validEmail)
        cy.get('input[name="platform"]').type(platformName)
        cy.get('button[type="submit"]').click()
      })

    cy.contains('Thanks for your input!').should('be.visible')
  })
  //   it('should test the third form with a valid email and platform name "Slack"', () => {
  //     const validEmail = 'test@example.com'
  //     const integrationName = 'Next Cloud'

  //     cy.get('form')
  //       .eq(1)
  //       .within(() => {
  //         cy.get('input[name="email"]').type(validEmail)
  //         cy.get('input[name="integration"]').type(integrationName)
  //         cy.get('button[type="submit"]').click()
  //       })

  //     cy.contains('Thanks for your input!').should('be.visible')
  //   })
})
export {}
